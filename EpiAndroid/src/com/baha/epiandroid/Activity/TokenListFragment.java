package com.baha.epiandroid.Activity;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.baha.epiandroid.Model.Planning;
import com.baha.epiandroid.Network.RequestTask;
import com.baha.epiandroid.R;
import com.baha.epiandroid.Utility.ExtendedSimpleAdapter;

import org.json.JSONException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class TokenListFragment extends Fragment {

    public TokenListFragment(){}

    private String _token = null;
    private Calendar cal = null;
    private Calendar cal2 = null;
    private ListView token_list = null;

    private ArrayList<HashMap<String, Object>> listItem;
    private HashMap<String, Object> map2;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_token, container, false);

        _token = getArguments().getString("token");
        token_list = (ListView) rootView.findViewById(R.id.token_listview);
        cal = Calendar.getInstance();
        cal2 = Calendar.getInstance();
        cal2.add(Calendar.WEEK_OF_MONTH, 1);

        showActivity(rootView);

        return rootView;
    }
    public void showActivity(View rootView) {

        RequestTask planningTask = new RequestTask(getResources().getString(R.string.urlApiPlanning), "GET");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String response = null;
        try {
            response = planningTask.execute("token", _token, "start", dateFormat.format(cal.getTime()), "end", dateFormat.format(cal2.getTime()), null).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        List<Planning> plannings = null;
        try {
            plannings = Planning.parses(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        listItem = new ArrayList<HashMap<String, Object>>();
        HashMap<String, Object> map2;

        for (Planning planning:plannings)
        {
            /* condition pour savoir si nous avons un token ou pas */
            if (!String.valueOf(planning.getallowToken()).equals("true"))
                    continue;
            map2 = new HashMap<String, Object>();

            map2.put("no", String.valueOf(planning.getActiTitle()));
            map2.put("one", "Room : " +String.valueOf(planning.getCodeRoom()));
            map2.put("two", planning.getStart());
            map2.put("three", planning.getEnd());
            map2.put("scolaryear", "Scolar year : " + planning.getScolarYear());
            map2.put("codemodule", "Code module : " + planning.getCodeModule());
            map2.put("codeinstance", "Code instance : " + planning.getCodeInstance());
            map2.put("codeacti", "Code activite : " + planning.getCodeActi());
            map2.put("codeevent", "Code event : " + planning.getCodeEvent());

            if (!String.valueOf(planning.getEventRegistered()).equals("null"))
                map2.put("img", String.valueOf(R.drawable.planning_activite_active));
            else if (String.valueOf(planning.getEventRegistered()).equals("null"))
                map2.put("img", String.valueOf(R.drawable.planning_activite_notactive));
            listItem.add(map2);
        }

        ExtendedSimpleAdapter mSchedule = new ExtendedSimpleAdapter(this.getActivity().getBaseContext(), listItem, R.layout.listview_item_token,
                new String[] { "no", "one", "two", "three", "img", "scolaryear","codemodule","codeinstance","codeacti", "codeevent",  }, new int[] {
                R.id.token_title, R.id.token_room, R.id.token_time_start, R.id.token_time_end, R.id.token_image_active, R.id.token_scolar_year,
                R.id.token_codemodule, R.id.token_codeinstance
                , R.id.token_codeacti, R.id.token_codeevent});
        token_list.setAdapter(mSchedule);

        if (listItem.isEmpty())
        {
            TextView change_name = (TextView) rootView.findViewById(R.id.txtLabel12);
            change_name.setTextColor(Color.RED);
            change_name.setText("No token validation !!!");
        }
        else
        {
            TextView change_name = (TextView) rootView.findViewById(R.id.txtLabel12);
            change_name.setTextColor(Color.BLUE);
            change_name.setText("List of token validation !!!");
        }

    }
}
