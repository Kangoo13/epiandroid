package com.baha.epiandroid.Activity;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.baha.epiandroid.Model.Module;
import com.baha.epiandroid.Network.RequestTask;
import com.baha.epiandroid.R;
import com.baha.epiandroid.Utility.ExtendedSimpleAdapter;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ModulesFragment extends Fragment {
	
	public ModulesFragment(){}

    private String _token;
    ListView list,list_head;
    ArrayList<HashMap<String, String>> mylist_title;
    ListAdapter adapter_title;
    HashMap<String, String> map1;
    String setCheckBox = "true";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        final View rootView = inflater.inflate(R.layout.fragment_modules, container, false);
        list = (ListView) rootView.findViewById(R.id.modules_listView2);
        list_head = (ListView) rootView.findViewById(R.id.modules_listView1);
        final CheckBox checkBox = (CheckBox) rootView.findViewById(R.id.modules_checkBox);

        showActivity(rootView, setCheckBox);

        checkBox.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(checkBox.isChecked())
                {
                    setCheckBox = "true";
                    showActivity(rootView, setCheckBox);
                }
                else {
                    setCheckBox = "false";
                    showActivity(rootView, setCheckBox);
                }
            }
        });

        return rootView;
    }

    public void showActivity(View rootView, String setCheckBox) {
        _token = getArguments().getString("token");

        RequestTask susieTask = new RequestTask(getResources().getString(R.string.urlApiModules), "GET");
        String response = null;
        try {
            response = susieTask.execute("token", _token, null).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        mylist_title = new ArrayList<HashMap<String, String>>();

        /**********Display the headings************/
        map1 = new HashMap<String, String>();
        map1.put("no", "Title");
        map1.put("one", " Inscription");
        map1.put("two", " Grade");
        map1.put("three", " Credits");
        map1.put("four", " Modules");
        mylist_title.add(map1);

        adapter_title = new SimpleAdapter(this.getActivity().getBaseContext(), mylist_title, R.layout.row_modules,
            new String[] { "no", "one", "two", "three", "four" }, new int[] {
                    R.id.modules_title, R.id.modules_inscription, R.id.modules_grade, R.id.modules_credits, R.id.modules_name_modules});
        list_head.setAdapter(adapter_title);

        List<Module> modules = null;
        try {
            modules = Module.parses(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ArrayList<HashMap<String, Object>> list_add_item = new ArrayList<HashMap<String, Object>>();
        HashMap<String, Object> map2;
        for (Module module:modules)
        {
            if (setCheckBox.equals("true"))
                if (!String.valueOf(module.getScolarYear()).equals("2014"))
                    continue;
            map2 = new HashMap<String, Object>();
            map2.put("no", module.getTitle());
            map2.put("one", String.valueOf(module.getDateIns()));
            map2.put("two", module.getGrade());
            map2.put("three", String.valueOf(module.getCredits()));
            map2.put("four", module.getCodeModule());
            list_add_item.add(map2);
        }
            ExtendedSimpleAdapter mSchedule = new ExtendedSimpleAdapter(this.getActivity().getBaseContext(), list_add_item, R.layout.row_modules,
                    new String[] { "no", "one", "two", "three", "four"}, new int[] {
                    R.id.modules_title, R.id.modules_inscription, R.id.modules_grade, R.id.modules_credits, R.id.modules_name_modules });
            list.setAdapter(mSchedule);
    }
}
