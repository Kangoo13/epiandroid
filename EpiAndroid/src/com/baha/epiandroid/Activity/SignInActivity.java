package com.baha.epiandroid.Activity;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import java.util.concurrent.ExecutionException;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import com.baha.epiandroid.Network.RequestTask;
import com.baha.epiandroid.R;

public class SignInActivity extends Activity {
  //  public ProgressDialog progress;
    public  ProgressDialog pd;
    @Override
    public void onBackPressed() {
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in_screen);
        CheckBox checkbox = (CheckBox)findViewById(R.id.checkBox);
        SharedPreferences prefs = this.getSharedPreferences(getResources().getString(R.string.name_source), Context.MODE_PRIVATE);
        if (prefs.contains("login") && prefs.contains("password"))
        {
            TextView login = (TextView) this.findViewById(R.id.etUserName);
            TextView password = (TextView) this.findViewById(R.id.etPass);
            login.setText(prefs.getString("login", "login"));
            password.setText(prefs.getString("password", "password"));
            checkbox.setChecked(true);
        }
        final Button button = (Button) findViewById(R.id.btnConnexion);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                EditText  login=null;
                EditText  pass=null;

                login = (EditText)findViewById(R.id.etUserName);
                pass = (EditText)findViewById(R.id.etPass);
                if(login.getText().toString().equals("") || pass.getText().toString().equals(""))
                {
                    if (pd != null && pd.isShowing())
                        pd.dismiss();
                    if (login.getText().toString().equals(""))
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_login),
                                Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_pass),
                                Toast.LENGTH_SHORT).show();
                }
               else {
                    try {
                        checkNetwork(login.getText().toString(), pass.getText().toString());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void checkNetwork(String login, String pass) throws InterruptedException, ExecutionException {
        //ProgressDialog.show(this, "Loading", "Wait while loading...");
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected())
        {
            sendJson(login, pass);
        }
        else
        {
            if (pd != null && pd.isShowing())
                pd.dismiss();
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_network),
                    Toast.LENGTH_SHORT).show();
        }
    }

    protected void sendJson(String login, String pwd) throws InterruptedException, ExecutionException {
        RequestTask task = new RequestTask(getResources().getString(R.string.urlApiLogin), "POST");
        String response = task.execute("login", login, "password", pwd, null).get();
        JSONObject token = null;
       Bundle b = new Bundle();
        try {
            token = new JSONObject(response);
            b.putString("token", token.getString("token"));
            CheckBox checkbox = (CheckBox)findViewById(R.id.checkBox);
            SharedPreferences prefs = this.getSharedPreferences(getResources().getString(R.string.name_source), Context.MODE_PRIVATE);
            if (checkbox.isChecked()) {
                prefs.edit().putString("login", login).apply();
                prefs.edit().putString("password", pwd).apply();
            }
            else
                prefs.edit().clear().apply();
            Intent intent = new Intent(SignInActivity.this, HomeFragmentActivity.class);
            intent.putExtras(b);
            startActivity(intent);
            this.finish();
        } catch (JSONException e) {
            assert token != null;
            try {
                Toast.makeText(getApplicationContext(), token.getJSONObject("error").getString("message"),
                        Toast.LENGTH_SHORT).show();
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }


    }
}
