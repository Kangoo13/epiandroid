package com.baha.epiandroid.Activity;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.baha.epiandroid.Model.Susie;
import com.baha.epiandroid.Network.RequestTask;
import com.baha.epiandroid.R;
import com.baha.epiandroid.Utility.ExtendedSimpleAdapter;

import org.json.JSONException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class SusieFragment extends Fragment {
    private String _token;
    private TextView textView;
    private String SetParmsSusie = "all";

	public SusieFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_susie, container, false);
        Update_susie(rootView, SetParmsSusie);
        /*     --------------------------------------------------------------      */
        final RadioGroup radioGroup2 = (RadioGroup) rootView.findViewById(R.id.radio_susie_type);
        textView = (TextView) rootView.findViewById(R.id.text);
        radioGroup2.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.button_susie_all) {
                    SetParmsSusie = "all";
                    textView.setText(getResources().getString(R.string.susie_all));
                    Update_susie(rootView, SetParmsSusie);

                } else if(checkedId == R.id.button_susie_free) {
                    SetParmsSusie = "free";
                    textView.setText(getResources().getString(R.string.susie_fre));
                    Update_susie(rootView, SetParmsSusie);

                } else {
                    SetParmsSusie = "registered";
                    textView.setText(getResources().getString(R.string.susie_registered));
                    Update_susie(rootView, SetParmsSusie);
                }
            }
        });
        return rootView;
    }

    void Update_susie(View rootView, String _SusieParams)
    {
        _token = getArguments().getString("token");
        RequestTask susieTask = new RequestTask(getResources().getString(R.string.urlApiSusie), "GET");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal2.add(Calendar.MONTH, 2);
        String response = null;
        try {
            response = susieTask.execute("token", _token, "start", dateFormat.format(cal.getTime()), "end", dateFormat.format(cal2.getTime()), "get", _SusieParams, null).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        List<Susie> susies = null;
        try {
            susies = Susie.parses(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ListView susie_list = (ListView) rootView.findViewById(R.id.list_susie);
        ArrayList<HashMap<String, Object>> listItem = new ArrayList<HashMap<String, Object>>();
        HashMap<String, Object> map;
        for (Susie susie:susies)
        {
            map = new HashMap<String, Object>();
            map.put("title", "Title : "+ susie.getTitle());
            map.put("makertitle", susie.getTitleMaker());
            map.put("type", "Type : " + susie.getType());
            map.put("time_start", "Start : " + susie.getStart());
            map.put("time_end", "End :   " + susie.getEnd());
            map.put("location", "Location : " + susie.getLocation() + "   Nb place : " + susie.getNbPlace());
            listItem.add(map);
        }
        ExtendedSimpleAdapter mSchedule = new ExtendedSimpleAdapter(this.getActivity().getBaseContext(), listItem, R.layout.listview_item_susie,
                new String[] {"title", "makertitle", "type", "time_start", "time_end", "location"}, new int[] {R.id.susie_title, R.id.susie_titlemaker, R.id.susie_type, R.id.susie_time_start,R.id.susie_time_end,R.id.susie_location});
        susie_list.setAdapter(mSchedule);
    }

}
