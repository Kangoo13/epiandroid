package com.baha.epiandroid.Activity;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.baha.epiandroid.Model.Message;
import com.baha.epiandroid.Network.ImageFromWeb;
import com.baha.epiandroid.Network.RequestTask;
import com.baha.epiandroid.R;
import com.baha.epiandroid.Utility.ExtendedSimpleAdapter;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class TrombiFragment extends Fragment {

    public TrombiFragment() {
    }

    private String _token;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_trombi, container, false);

        Func_trombi(rootView);
        return rootView;
    }

    void Func_trombi(View rootView) {
        _token = getArguments().getString("token");
        RequestTask MessageTask = new RequestTask("http://epitech-api.herokuapp.com/trombi", "GET");
        String response_trombi = null;

        try {
            response_trombi = MessageTask.execute("token", _token, "year", "2014", "location", "FR/NCE",  null).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        TextView change_name = (TextView) rootView.findViewById(R.id.trombi_text);
        change_name.setText(response_trombi);
    }
}
