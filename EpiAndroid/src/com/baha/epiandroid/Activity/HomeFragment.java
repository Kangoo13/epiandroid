package  com.baha.epiandroid.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.baha.epiandroid.Model.Informations;
import com.baha.epiandroid.Model.Message;
import com.baha.epiandroid.Model.Utilisateur;
import com.baha.epiandroid.Network.RequestTask;
import com.baha.epiandroid.Network.*;
import com.baha.epiandroid.R;
import com.baha.epiandroid.Utility.ExtendedSimpleAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by RatJat on 15/01/2015.
 */

public class HomeFragment extends Fragment {

	public HomeFragment(){
    }

    private String _token;
    ListAdapter adapter;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        _token = getArguments().getString("token");
        RequestTask HomeTask = new RequestTask(getResources().getString(R.string.urlApiInfos), "POST");
        RequestTask MessageTask = new RequestTask(getResources().getString(R.string.urlApiMessage), "GET");

        String response = null;
        try {
            response = HomeTask.execute("token", _token, null).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        String response_message = null;
        try {
            response_message = MessageTask.execute("token", _token, null).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        Informations informations = null;
        try {
            informations = Informations.parse(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Utilisateur utilisateur = informations.getUtilisateur();
        List<Message> messages = null;
        try {
            messages = Message.parses(response_message);
        } catch (JSONException e) {
            e.printStackTrace();
        }

            TextView change_name = (TextView) rootView.findViewById(R.id.surname);
            ListView message_list = (ListView) rootView.findViewById(R.id.list_message);
            ListView user_list = (ListView) rootView.findViewById(R.id.list_user);
            change_name.setText(utilisateur.getTitle());

            ArrayList<HashMap<String, Object>> listItem = new ArrayList<HashMap<String, Object>>();
            ArrayList<HashMap<String, Object>> mylist_user_home= new ArrayList<HashMap<String, Object>>();
            HashMap<String, Object> map;
            HashMap<String, Object>map2;

            map2 = new HashMap<String, Object>();
            map2.put("login", "Login : " + utilisateur.getLogin());

            map2.put("email", "Email : " + utilisateur.getInternalEmail());
            map2.put("ip", "Ip : " + informations.getIp());
            map2.put("log", "Log : "+ informations.getActiveLog());
            map2.put("promo", "Promo : "+ utilisateur.getPromo());
            map2.put("year", "Year : "+ utilisateur.getStudentYear());
            map2.put("semester", "Semester : "+ informations.getSemesterCode());
            mylist_user_home.add(map2);
            ExtendedSimpleAdapter mSchedule2 = new ExtendedSimpleAdapter(this.getActivity().getBaseContext(), mylist_user_home, R.layout.listview_item_home_user,
                    new String[] { "login", "email", "ip", "log", "promo", "year", "semester"}, new int[] {R.id._login, R.id._email, R.id._ip, R.id._log, R.id._promo, R.id._year, R.id._semester });
            user_list.setAdapter(mSchedule2);

           for (Message message:messages)
            {

                map = new HashMap<String, Object>();
                map.put("title", android.text.Html.fromHtml((message.getTitle())).toString());
                map.put("content",android.text.Html.fromHtml((message.getContent()).toString()));
                map.put("date", message.getDate());
                if (message.getUserPicture().equals("null"))
                    map.put("img", String.valueOf(R.drawable.message_user_picture));
                else {
                    ImageFromWeb imageFromWeb = new ImageFromWeb();
                    Bitmap bitmap = null;
                    try {
                        bitmap = imageFromWeb.execute(message.getUserPicture()).get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                    map.put("img", bitmap);
                }
                listItem.add(map);
            }

        ExtendedSimpleAdapter mSchedule = new ExtendedSimpleAdapter(this.getActivity().getBaseContext(), listItem, R.layout.listview_item_home_message,
                new String[] {"title", "content", "date", "img"}, new int[] {R.id.message_title, R.id.message_content, R.id.message_date, R.id.message_image});
        message_list.setAdapter(mSchedule);
        setimageuser(utilisateur, rootView);
        return rootView;
    }

    public void setimageuser( Utilisateur utilisateur, View rootView ) {
        RequestTask photo_user = new RequestTask(getResources().getString(R.string.urlApiPhoto), "GET");
        String response_photo_user = null;
        try {
            response_photo_user = photo_user.execute("token", _token, "login", utilisateur.getLogin(), null).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        JSONObject json_photo_user = null;
        try {
            json_photo_user = new JSONObject(response_photo_user);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            String url_photo_user = json_photo_user.getString("url");
            ImageFromWeb imageFromWeb = new ImageFromWeb((ImageView) rootView.findViewById(R.id.image_user));
            imageFromWeb.execute(url_photo_user);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}