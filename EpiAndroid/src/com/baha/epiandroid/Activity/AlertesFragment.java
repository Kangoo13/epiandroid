package com.baha.epiandroid.Activity;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import org.json.JSONArray;
import com.baha.epiandroid.Network.RequestTask;
import com.baha.epiandroid.R;
import com.baha.epiandroid.Utility.ExtendedSimpleAdapter;
import org.json.JSONException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class AlertesFragment extends Fragment {
	
	public AlertesFragment(){}
    private String _token;
    ListView alertes_list = null;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_alertes, container, false);
        alertes_list = (ListView) rootView.findViewById(R.id.list_alertes);
        Update_alertes(rootView);
        return rootView;
    }
    void Update_alertes(View rootView)
    {
        _token = getArguments().getString("token");
        RequestTask susieTask = new RequestTask(getResources().getString(R.string.urlApiAlerts), "GET");

        String response = null;
        try {
            response = susieTask.execute("token", _token, null).get();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        JSONArray arr = null;
        try {
            arr = new JSONArray(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        List<String> list = new ArrayList<String>();
        for(int i = 0; i < arr.length(); i++){
            try {
                list.add(arr.getJSONObject(i).getString("title"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ArrayList<HashMap<String, Object>> listItem = new ArrayList<HashMap<String, Object>>();
        HashMap<String, Object> map;

        for (String x:list)
        {
            map = new HashMap<String, Object>();
            map.put("title", String.valueOf(x));
            map.put("img", String.valueOf(R.drawable.myalertes));

            listItem.add(map);
        }
        ExtendedSimpleAdapter mSchedule = new ExtendedSimpleAdapter(this.getActivity().getBaseContext(), listItem, R.layout.listview_item_alertes,
                new String[] {"title", "img"}, new int[] {R.id.alertes_text, R.id.alertes_image});
        alertes_list.setAdapter(mSchedule);
    }

}
