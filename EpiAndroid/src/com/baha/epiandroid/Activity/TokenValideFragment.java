

/**
 * Created by RatJat on 01/02/2015.
 */

package com.baha.epiandroid.Activity;

        import android.app.Fragment;
        import android.os.Bundle;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.Button;
        import android.widget.CheckBox;
        import android.widget.EditText;
        import android.widget.Toast;

        import com.baha.epiandroid.Network.RequestTask;
        import com.baha.epiandroid.R;

        import java.util.concurrent.ExecutionException;

public class TokenValideFragment  extends Fragment implements View.OnClickListener {

    private EditText module = null;
    private EditText event = null;
    private EditText acti = null;
    private EditText instance = null;
    private EditText token = null;
    private String _token = null;
    public TokenValideFragment (){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_token_validation, container, false);
        Button button = (Button) rootView.findViewById(R.id.btn_valide_token);
        module = (EditText) rootView.findViewById(R.id.token_codemodule_validation);
        event = (EditText) rootView.findViewById(R.id.token_codeevent_validation);
        acti = (EditText) rootView.findViewById(R.id.token_codeacti_validation);
        instance = (EditText) rootView.findViewById(R.id.token_codeinstance_validation);
        token = (EditText) rootView.findViewById(R.id.token_codenumber_validation);
        _token = getArguments().getString("token");
        button.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        if (module.getText().toString().equals(""))
            Toast.makeText(this.getActivity().getBaseContext(), getResources().getString(R.string.token_module),
                    Toast.LENGTH_SHORT).show();
        else if (event.getText().toString().equals(""))
            Toast.makeText(this.getActivity().getBaseContext(), getResources().getString(R.string.token_event),
                    Toast.LENGTH_SHORT).show();
        else if (acti.getText().toString().equals(""))
            Toast.makeText(this.getActivity().getBaseContext(), getResources().getString(R.string.token_acti),
                    Toast.LENGTH_SHORT).show();
        else if (instance.getText().toString().equals(""))
            Toast.makeText(this.getActivity().getBaseContext(), getResources().getString(R.string.token_instance),
                    Toast.LENGTH_SHORT).show();
        else if (token.getText().toString().equals(""))
            Toast.makeText(this.getActivity().getBaseContext(), getResources().getString(R.string.token_token),
                    Toast.LENGTH_SHORT).show();
        else
        {
            RequestTask task = new RequestTask(getResources().getString(R.string.urlApiToken), "POST");
            String response = null;
            try {
                Toast.makeText(this.getActivity().getBaseContext(), _token,
                        Toast.LENGTH_SHORT).show();
                response = task.execute("token", _token, "scolaryear", "2014", "codemodule", module.getText().toString(), "codeinstance", instance.getText().toString(), "codeacti", acti.getText().toString(), "codeevent", event.getText().toString(), "tokenvalidationcode", token.getText().toString(),  null).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            Toast.makeText(this.getActivity().getBaseContext(), response,
                    Toast.LENGTH_SHORT).show();
        }

    }
}
