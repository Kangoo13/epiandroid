package com.baha.epiandroid.Activity;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.baha.epiandroid.Model.Project;
import com.baha.epiandroid.Network.RequestTask;
import com.baha.epiandroid.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import com.baha.epiandroid.R;
import com.baha.epiandroid.Utility.ExtendedSimpleAdapter;

import org.json.JSONException;

public class ProjectsFragment extends Fragment {
	
	public ProjectsFragment(){}
    private String _token;
    ListView list,list_head;
    ArrayList<HashMap<String, String>> mylist, mylist_title;
    ListAdapter adapter_title;
    HashMap<String, String> map1, map2;
    String setCheckBox = "true";

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_projets, container, false);
        list = (ListView) rootView.findViewById(R.id.projet_listView2);
        list_head = (ListView) rootView.findViewById(R.id.projet_listView1);
        final CheckBox checkBox = (CheckBox) rootView.findViewById(R.id.myproject_checkBox);

        showActivity(setCheckBox);

        checkBox.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(checkBox.isChecked())
                {
                    setCheckBox = "true";
                    showActivity(setCheckBox);
                }
                else {
                    setCheckBox = "false";
                    showActivity(setCheckBox);
                }
             }
        });

        return rootView;
    }
    public void showActivity(String setCheckBox) {

        _token = getArguments().getString("token");
        RequestTask projectTask = new RequestTask(getResources().getString(R.string.urlApiProjects), "GET");
        String response = null;
        try {
            response = projectTask.execute("token", _token, null).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        mylist = new ArrayList<HashMap<String, String>>();
        mylist_title = new ArrayList<HashMap<String, String>>();

        /**********Display the headings************/
        map1 = new HashMap<String, String>();
        map1.put("no", "Activité");
        map1.put("one", " Début");
        map1.put("two", " Fin");
        map1.put("three", " Type");
        map1.put("four", " Modules");
        mylist_title.add(map1);

        try {
            adapter_title = new SimpleAdapter(this.getActivity().getBaseContext(), mylist_title, R.layout.row_projet,
                    new String[] { "no", "one", "two", "three", "four" }, new int[] {
                    R.id.projet_activite, R.id.projet_debut, R.id.projet_fin, R.id.projet_type, R.id.projet_module});
            list_head.setAdapter(adapter_title);
        } catch (Exception e) {

        }

        List<Project> projects = null;
        try {
            projects = Project.parses(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ArrayList<HashMap<String, Object>> list_add_item = new ArrayList<HashMap<String, Object>>();
        HashMap<String, Object> map2;
        for (Project project:projects)
        {
            if (setCheckBox.equals("true"))
                if (String.valueOf(project.getRegistered()).equals("0"))
                    continue;
            map2 = new HashMap<String, Object>();
            map2.put("no", project.getActiTitle());
            map2.put("one", project.getBeginActi());
            map2.put("two", project.getEndActi());
            map2.put("three", project.getTypeActi());
            map2.put("four", project.getTitleModule());
            list_add_item.add(map2);
        }
            ExtendedSimpleAdapter mSchedule = new ExtendedSimpleAdapter(this.getActivity().getBaseContext(), list_add_item, R.layout.row_projet,
                    new String[] { "no", "one", "two", "three", "four"}, new int[] {
                    R.id.projet_activite, R.id.projet_debut, R.id.projet_fin, R.id.projet_type, R.id.projet_module });
            list.setAdapter(mSchedule);
    }
}
