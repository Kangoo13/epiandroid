package com.baha.epiandroid.Activity;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.baha.epiandroid.Model.Message;
import com.baha.epiandroid.Network.ImageFromWeb;
import com.baha.epiandroid.Network.RequestTask;
import com.baha.epiandroid.R;
import com.baha.epiandroid.Utility.ExtendedSimpleAdapter;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MessageFragment extends Fragment {
	
	public MessageFragment(){}

    private String _token;

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_messages, container, false);
        Func_message(rootView);
        return rootView;
    }

    void Func_message(View rootView)
    {
        _token = getArguments().getString("token");
        RequestTask MessageTask = new RequestTask(getResources().getString(R.string.urlApiMessage), "GET");
        String response_message = null;
        ListView message_list = (ListView) rootView.findViewById(R.id.item_list_message);
        try {
            response_message = MessageTask.execute("token", _token, null).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        List<Message> messages = null;
        try {
            messages = Message.parses(response_message);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ArrayList<HashMap<String, Object>> listItem = new ArrayList<HashMap<String, Object>>();
        HashMap<String, Object> map;

        for (Message message:messages)
        {

            map = new HashMap<String, Object>();
            map.put("title", android.text.Html.fromHtml((message.getTitle())).toString());
            map.put("content",android.text.Html.fromHtml((message.getContent()).toString()));
            map.put("date", message.getDate());
            if (message.getUserPicture().equals("null"))
                map.put("img", String.valueOf(R.drawable.message_user_picture));
            else {
                ImageFromWeb imageFromWeb = new ImageFromWeb();
                Bitmap bitmap = null;
                try {
                    bitmap = imageFromWeb.execute(message.getUserPicture()).get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                map.put("img", bitmap);
            }
            listItem.add(map);
        }

        ExtendedSimpleAdapter mSchedule = new ExtendedSimpleAdapter(this.getActivity().getBaseContext(), listItem, R.layout.listview_item_home_message,
                new String[] {"title", "content", "date", "img"}, new int[] {R.id.message_title, R.id.message_content, R.id.message_date, R.id.message_image});
        message_list.setAdapter(mSchedule);
    }
}
