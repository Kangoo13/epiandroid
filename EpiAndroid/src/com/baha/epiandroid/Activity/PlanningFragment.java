package com.baha.epiandroid.Activity;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.baha.epiandroid.Model.Planning;
import com.baha.epiandroid.Model.Susie;
import com.baha.epiandroid.Network.RequestTask;
import com.baha.epiandroid.R;
import com.baha.epiandroid.Utility.ExtendedSimpleAdapter;

import org.json.JSONException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class PlanningFragment extends Fragment implements View.OnClickListener{
	
	public PlanningFragment(){}

    private String _token = null;
    private Calendar cal = null;
    private Calendar cal2 = null;
    private TextView date_previous, date_next = null;
    private ListView planning_list = null;

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_planning, container, false);
        _token = getArguments().getString("token");
        Button btnnext = (Button) rootView.findViewById(R.id.btn_planning_nxt);
        Button btnprevisou = (Button) rootView.findViewById(R.id.btn_planning_pre);
        date_previous = (TextView) rootView.findViewById(R.id.planning_date_start);
        date_next = (TextView) rootView.findViewById(R.id.planning_date_end);
        planning_list = (ListView) rootView.findViewById(R.id.planning_listview);

        btnnext.setOnClickListener(this);
        btnprevisou.setOnClickListener(this);

        cal = Calendar.getInstance();
        cal2 = Calendar.getInstance();
        cal2.add(Calendar.WEEK_OF_MONTH, 1);
        showActivity(rootView);
        return rootView;
    }

    public void showActivity(View rootView) {

        RequestTask planningTask = new RequestTask(getResources().getString(R.string.urlApiPlanning), "GET");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String response = null;
        try {
            response = planningTask.execute("token", _token, "start", dateFormat.format(cal.getTime()), "end", dateFormat.format(cal2.getTime()), null).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        List<Planning> plannings = null;
        try {
            plannings = Planning.parses(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        date_previous.setText(String.valueOf(dateFormat.format(cal.getTime())));
        date_next.setText(String.valueOf(dateFormat.format(cal2.getTime())));

        ArrayList<HashMap<String, Object>> listItem = new ArrayList<HashMap<String, Object>>();
        HashMap<String, Object> map2;

        for (Planning planning:plannings)
        {
            map2 = new HashMap<String, Object>();

            map2.put("no", String.valueOf(planning.getActiTitle()));
            map2.put("one", "Room : " +String.valueOf(planning.getCodeRoom()));
            map2.put("two", planning.getStart());
            map2.put("three", planning.getEnd());
            if (!String.valueOf(planning.getEventRegistered()).equals("null"))
              map2.put("img", String.valueOf(R.drawable.planning_activite_active));
            else if (String.valueOf(planning.getEventRegistered()).equals("null"))
                map2.put("img", String.valueOf(R.drawable.planning_activite_notactive));
            listItem.add(map2);
        }
            ExtendedSimpleAdapter mSchedule = new ExtendedSimpleAdapter(this.getActivity().getBaseContext(), listItem, R.layout.listview_item_planning,
                    new String[] { "no", "one", "two", "three", "img"}, new int[] {
                    R.id.planning_title, R.id.planning_room, R.id.planning_time_start, R.id.planning_time_end, R.id.planning_image_active });
            planning_list.setAdapter(mSchedule);
    }

    @Override
    public void onClick(View v) {

        switch(v.getId()){
            case R.id.btn_planning_nxt:
                cal.add(Calendar.WEEK_OF_YEAR, 1);
                cal2.add(Calendar.WEEK_OF_YEAR, 1);
                showActivity(v);
                break;
            case R.id.btn_planning_pre:
                cal.add(Calendar.WEEK_OF_YEAR, -1);
                cal2.add(Calendar.WEEK_OF_YEAR, -1);
                showActivity(v);
                break;
        }

    }
}
