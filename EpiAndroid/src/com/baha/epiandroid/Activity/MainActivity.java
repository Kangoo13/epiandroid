package com.baha.epiandroid.Activity;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.baha.epiandroid.R;

public class MainActivity extends Activity implements OnClickListener {

    Button btnSignIn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent i = new Intent(this,SignInActivity.class);
        startActivity(i);
        btnSignIn = (Button) findViewById(R.id.btnConnexion);
        btnSignIn.setOnClickListener(this);
    }

    public void onClick(View v) {
        Intent i = null;
        switch(v.getId()){
            case R.id.btnConnexion:
                i = new Intent(this,SignInActivity.class);
                break;
        }
        startActivity(i);
    }
}
