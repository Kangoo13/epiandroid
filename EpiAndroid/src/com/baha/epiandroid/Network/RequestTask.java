package com.baha.epiandroid.Network;

import android.os.AsyncTask;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by KangooRou on 14/01/2015.
 */
public class RequestTask extends AsyncTask<String, Void, String> {

    private String _url;
    private String _httpType;

    public RequestTask(String url, String type)
    {
        _url = url;
        _httpType = type;
    }
    @Override
    protected String doInBackground(String... params) {
        try {
            String response = null;
            if (_httpType.equals("POST")) {
                HttpPost httpPost = new HttpPost(_url);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                for (int i = 0; params[i] != null; i += 2) {
                    nameValuePairs.add(new BasicNameValuePair(params[i], params[i + 1]));
                }
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpClient httpClient = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 10000); //Timeout Limit

                // Execute HTTP Post Request
                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                response = EntityUtils.toString(httpEntity);
                Header[] allHeaders = httpResponse.getAllHeaders();

            }
            else if (_httpType.equals("GET"))
            {

                for (int i = 0; params[i] != null; i += 2) {
                    if (i == 0)
                        _url += "?" ;
                    else
                        _url += "&";
                    _url += params[i] + "=" + params[i+1];
                }
                HttpGet httpGet = new HttpGet(_url);
                HttpClient httpClient = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 10000); //Timeout Limit

                // Execute HTTP Post Request
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                response = EntityUtils.toString(httpEntity);
                Header[] allHeaders = httpResponse.getAllHeaders();

            }
            //System.out.println("RequestTask url: " + _url + " Response : " + response);
            return response;
        } catch (ClientProtocolException e) {
            //  Toast.makeText(getApplicationContext(), "rep impossible]",
            //          Toast.LENGTH_SHORT).show();
            // TODO Auto-generated catch block
        } catch (IOException e) {
            //Toast.makeText(getApplicationContext(), (String)e.printStackTrace(),
            // Toast.LENGTH_SHORT).show();
            // TODO Auto-generated catch block
        }
        return null;
    }
}