package com.baha.epiandroid.Network;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by KangooRou on 26/01/2015.
 */
public class ImageFromWeb extends AsyncTask<String, Void, Bitmap> {
    ImageView bmImage = null;

    public ImageFromWeb(ImageView bmImage) {
        this.bmImage = bmImage;
    }
    public ImageFromWeb() {}

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        try {

            URL url = new URL(urldisplay);
            InputStream in = url.openConnection().getInputStream();
            BufferedInputStream bis = new BufferedInputStream(in,1024*8);
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            int len=0;
            byte[] buffer = new byte[1024];
            while((len = bis.read(buffer)) != -1){
                out.write(buffer, 0, len);
            }
            out.close();
            bis.close();

            byte[] data = out.toByteArray();
            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
           return bitmap;
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    protected void onPostExecute(Bitmap result) {
        if (bmImage != null)
        bmImage.setImageBitmap(result);
    }
}