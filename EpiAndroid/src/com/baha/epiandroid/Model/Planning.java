package com.baha.epiandroid.Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KangooRou on 31/01/2015.
 */
public class Planning {
    private List<Utilisateur> _profInst;
    private String _title;
    private String _rdvIndivRegistered;
    private String _allowedPlanningEnd;
    private int _nbGroup;
    private String _start;
    private String _registerMonth;
    private String _allowedPlanningStart;
    private boolean _project;
    private String _eventRegistered;
    private int _totalStudentsRegistered;
    private boolean _allowRegister;
    private String _codeModule;
    private String _rdvGroupRegistered;
    private String _semester;
    private String _typeCode;
    private int _isRdv;
    private boolean _allowToken;
    private String _titleModule;
    private boolean _inMoreThanOneMonth;
    private String _actiTitle;
    private String _instanceLocation;
    private String _nbHours;
    private String _registerProf;
    private String _nbMaxStudentsProjet;
    private String _typeRoom;
    private int _seatsRoom;
    private String _codeRoom;
    private String _codeActi;
    private String _codeEvent;
    private String _codeInstance;
    private String _dates;
    private boolean _registerStudent;
    private String _typeTitle;
    private int _numEvent;
    private String _end;
    private int _scolarYear;
    private boolean _moduleRegistered;
    private boolean _past;
    private boolean _moduleAvailable;


    public List<Utilisateur> getProfInst() {
        return _profInst;
    }

    public void setProfInst(List<Utilisateur> _profInst) {
        this._profInst = _profInst;
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String _title) {
        this._title = _title;
    }

    public String getRdvIndivRegistered() {
        return _rdvIndivRegistered;
    }

    public void setRdvIndivRegistered(String _rdvIndivRegistered) {
        this._rdvIndivRegistered = _rdvIndivRegistered;
    }

    public String getAllowedPlanningEnd() {
        return _allowedPlanningEnd;
    }

    public void setAllowedPlanningEnd(String _allowedPlanningEnd) {
        this._allowedPlanningEnd = _allowedPlanningEnd;
    }

    public int getNbGroup() {
        return _nbGroup;
    }

    public void setNbGroup(int _nbGroup) {
        this._nbGroup = _nbGroup;
    }

    public String getStart() {
        return _start;
    }

    public void setStart(String _start) {
        this._start = _start;
    }

    public String getRegisterMonth() {
        return _registerMonth;
    }

    public void setRegisterMonth(String _registerMonth) {
        this._registerMonth = _registerMonth;
    }

    public String getAllowedPlanningStart() {
        return _allowedPlanningStart;
    }

    public void setAllowedPlanningStart(String _allowedPlanningStart) {
        this._allowedPlanningStart = _allowedPlanningStart;
    }

    public boolean getproject() {
        return _project;
    }

    public void setProject(boolean _project) {
        this._project = _project;
    }

    public String getEventRegistered() {
        return _eventRegistered;
    }

    public void setEventRegistered(String _eventRegistered) {
        this._eventRegistered = _eventRegistered;
    }

    public int getTotalStudentsRegistered() {
        return _totalStudentsRegistered;
    }

    public void setTotalStudentsRegistered(int _totalStudentsRegistered) {
        this._totalStudentsRegistered = _totalStudentsRegistered;
    }

    public boolean getallowRegister() {
        return _allowRegister;
    }

    public void setAllowRegister(boolean _allowRegister) {
        this._allowRegister = _allowRegister;
    }

    public String getCodeModule() {
        return _codeModule;
    }

    public void setCodeModule(String _codeModule) {
        this._codeModule = _codeModule;
    }

    public String getRdvGroupRegistered() {
        return _rdvGroupRegistered;
    }

    public void setRdvGroupRegistered(String _rdvGroupRegistered) {
        this._rdvGroupRegistered = _rdvGroupRegistered;
    }

    public String getSemester() {
        return _semester;
    }

    public void setSemester(String _semester) {
        this._semester = _semester;
    }

    public String getTypeCode() {
        return _typeCode;
    }

    public void setTypeCode(String _typeCode) {
        this._typeCode = _typeCode;
    }

    public int getIsRdv() {
        return _isRdv;
    }

    public void setIsRdv(int _isRdv) {
        this._isRdv = _isRdv;
    }

    public boolean getallowToken() {
        return _allowToken;
    }

    public void setAllowToken(boolean _allowToken) {
        this._allowToken = _allowToken;
    }

    public String getTitleModule() {
        return _titleModule;
    }

    public void setTitleModule(String _titleModule) {
        this._titleModule = _titleModule;
    }

    public boolean getinMoreThanOneMonth() {
        return _inMoreThanOneMonth;
    }

    public void setInMoreThanOneMonth(boolean _inMoreThanOneMonth) {
        this._inMoreThanOneMonth = _inMoreThanOneMonth;
    }

    public String getActiTitle() {
        return _actiTitle;
    }

    public void setActiTitle(String _actiTitle) {
        this._actiTitle = _actiTitle;
    }

    public String getInstanceLocation() {
        return _instanceLocation;
    }

    public void setInstanceLocation(String _instanceLocation) {
        this._instanceLocation = _instanceLocation;
    }

    public String getNbHours() {
        return _nbHours;
    }

    public void setNbHours(String _nbHours) {
        this._nbHours = _nbHours;
    }

    public String getRegisterProf() {
        return _registerProf;
    }

    public void setRegisterProf(String _registerProf) {
        this._registerProf = _registerProf;
    }

    public String getNbMaxStudentsProjet() {
        return _nbMaxStudentsProjet;
    }

    public void setNbMaxStudentsProjet(String _nbMaxStudentsProjet) {
        this._nbMaxStudentsProjet = _nbMaxStudentsProjet;
    }

    public String getTypeRoom() {
        return _typeRoom;
    }

    public void setTypeRoom(String _typeRoom) {
        this._typeRoom = _typeRoom;
    }

    public int getSeatsRoom() {
        return _seatsRoom;
    }

    public void setSeatsRoom(int _seatsRoom) {
        this._seatsRoom = _seatsRoom;
    }

    public String getCodeRoom() {
        return _codeRoom;
    }

    public void setCodeRoom(String _codeRoom) {
        this._codeRoom = _codeRoom;
    }

    public String getCodeActi() {
        return _codeActi;
    }

    public void setCodeActi(String _codeActi) {
        this._codeActi = _codeActi;
    }

    public String getCodeEvent() {
        return _codeEvent;
    }

    public void setCodeEvent(String _codeEvent) {
        this._codeEvent = _codeEvent;
    }

    public String getCodeInstance() {
        return _codeInstance;
    }

    public void setCodeInstance(String _codeInstance) {
        this._codeInstance = _codeInstance;
    }

    public String get_dates() {
        return _dates;
    }

    public void set_dates(String _dates) {
        this._dates = _dates;
    }

    public boolean getregisterStudent() {
        return _registerStudent;
    }

    public void setRegisterStudent(boolean _registerStudent) {
        this._registerStudent = _registerStudent;
    }

    public String getTypeTitle() {
        return _typeTitle;
    }

    public void setTypeTitle(String _typeTitle) {
        this._typeTitle = _typeTitle;
    }

    public int getNumEvent() {
        return _numEvent;
    }

    public void setNumEvent(int _numEvent) {
        this._numEvent = _numEvent;
    }

    public String getEnd() {
        return _end;
    }

    public void setEnd(String _end) {
        this._end = _end;
    }

    public int getScolarYear() {
        return _scolarYear;
    }

    public void setScolarYear(int _scolarYear) {
        this._scolarYear = _scolarYear;
    }

    public boolean getmoduleRegistered() {
        return _moduleRegistered;
    }

    public void setModuleRegistered(boolean _moduleRegistered) {
        this._moduleRegistered = _moduleRegistered;
    }

    public boolean getpast() {
        return _past;
    }

    public void setPast(boolean _past) {
        this._past = _past;
    }

    public boolean getmoduleAvailable() {
        return _moduleAvailable;
    }

    public void setModuleAvailable(boolean _moduleAvailable) {
        this._moduleAvailable = _moduleAvailable;
    }

    static public List<Planning> parses(String json) throws JSONException {
        List<Planning> plannings = new ArrayList<Planning>();
        JSONArray array =  new JSONArray(json);
        int numberOfItems = array.length();
        JSONObject obj = null;
        JSONObject room = null;
        String prof = null;
        for (int i = 0; i != numberOfItems ; i++)
        {
            Planning planning = new Planning();
            obj = array.getJSONObject(i);

            if (obj.has("title"))
            planning.setActiTitle(obj.getString("title"));
            if (obj.has("rdv_indiv_registered"))
            planning.setRdvIndivRegistered(obj.getString("rdv_indiv_registered"));
            if (obj.has("allowed_planning_end"))
            planning.setAllowedPlanningEnd(obj.getString("allowed_planning_end"));
            if (obj.has("start"))
            planning.setStart(obj.getString("start"));
            if (obj.has("register_month"))
            planning.setRegisterMonth(obj.getString("register_month"));
            if (obj.has("allowed_planning_start"))
            planning.setAllowedPlanningStart(obj.getString("allowed_planning_start"));
            if (obj.has("event_registered"))
            planning.setEventRegistered(obj.getString("event_registered"));
            if (obj.has("codemodule"))
            planning.setCodeModule(obj.getString("codemodule"));
            if (obj.has("rdv_group_registered"))
            planning.setRdvGroupRegistered(obj.getString("rdv_group_registered"));
            if (obj.has("semester"))
            planning.setSemester(obj.getString("semester"));
            if (obj.has("type_code"))
            planning.setTypeCode(obj.getString("type_code"));
            if (obj.has("titlemodule"))
            planning.setTitleModule(obj.getString("titlemodule"));
            if (obj.has("acti_title"))
            planning.setActiTitle(obj.getString("acti_title"));
            if (obj.has("instance_location"))
            planning.setInstanceLocation(obj.getString("instance_location"));
            if (obj.has("nb_hours"))
            planning.setNbHours(obj.getString("nb_hours"));
            if (obj.has("register_prof"))
            planning.setRegisterProf(obj.getString("register_prof"));
            if (obj.has("codeacti"))
            planning.setCodeActi(obj.getString("codeacti"));
            if (obj.has("codeevent"))
            planning.setCodeEvent(obj.getString("codeevent"));
            if (obj.has("codeinstance"))
            planning.setCodeInstance(obj.getString("codeinstance"));
            if (obj.has("dates"))
            planning.set_dates(obj.getString("dates"));
            if (obj.has("type_title"))
            planning.setTypeTitle(obj.getString("type_title"));
            if (obj.has("end"))
            planning.setEnd(obj.getString("end"));

            if (obj.has("nb_group"))
            planning.setNbGroup(obj.getInt("nb_group"));
            if (obj.has("total_students_registered"))
            planning.setTotalStudentsRegistered(obj.getInt("total_students_registered"));
            if (obj.has("is_rdv"))
            planning.setIsRdv(obj.getInt("is_rdv"));
            if (obj.has("nb_max_students_projet"))
            planning.setNbMaxStudentsProjet(obj.getString("nb_max_students_projet"));
            if (obj.has("num_event"))
            planning.setNumEvent(obj.getInt("num_event"));
            if (obj.has("scolaryear"))
            planning.setScolarYear(obj.getInt("scolaryear"));
            if (obj.has("type_room"))
                planning.setTypeRoom(obj.getString("type_room"));

            if (obj.has("project"))
            planning.setProject(obj.getBoolean("project"));
            if (obj.has("allow_register"))
            planning.setAllowRegister(obj.getBoolean("allow_register"));
            if (obj.has("allow_token"))
            planning.setAllowToken(obj.getBoolean("allow_token"));
            if (obj.has("in_more_than_one_month"))
            planning.setInMoreThanOneMonth(obj.getBoolean("in_more_than_one_month"));
            if (obj.has("register_student"))
            planning.setRegisterStudent(obj.getBoolean("register_student"));
            if (obj.has("module_registered"))
            planning.setModuleRegistered(obj.getBoolean("module_registered"));
            if (obj.has("past"))
            planning.setPast(obj.getBoolean("past"));
            if (obj.has("module_available"))
            planning.setModuleAvailable(obj.getBoolean("module_available"));

            if (obj.has("room")) {
                room = obj.getJSONObject("room");
                if (room.has("type"))
                    planning.setTypeRoom(room.getString("type"));
                if (room.has("code"))
                    planning.setCodeRoom(room.getString("code"));
                if (room.has("seats"))
                    planning.setSeatsRoom(room.getInt("seats"));
            }


            if (obj.has("prof_inst")) {
                if (!obj.getString("prof_inst").equals("null"))
                prof = obj.getJSONArray("prof_inst").toString();
                planning.setProfInst(Utilisateur.parses(prof));
            }
            plannings.add(planning);

        }




        return plannings;
    }

}
