package com.baha.epiandroid.Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KangooRou on 19/01/2015.
 * Model class User
 */
public class Utilisateur {
    private int _id;
    private String _ip;
    private String _activeLog;
    private int _creditsMin;
    private int _creditsNorm;
    private int _creditsObj;
    private int _nslogMin;
    private int _nslogNorm;
    private String _semesterCode;
    private int _semesterNum;
    private int _achieved;
    private int _failed;
    private int _inProgress;
    private String _login;
    private String _title;
    private String _email;
    private String _internalEmail;
    private String _lastName;
    private String _firstName;
    private boolean _referentUsed;
    private String _picture;
    private String _pictureFun;
    private String _emailReferent;
    private String _passReferent;
    private int _promo;
    private String _semester;
    private int _uid;
    private int _gid;
    private String _location;
    private String _documents;
    private String _userdocs;
    private String _shell;
    private String _netsoul;
    private boolean _close;
    private String _closeReason;
    private String _ctime;
    private String _mtime;
    private String _comment;
    private int _idPromo;
    private int _idHistory;
    private String _courseCode;
    private String _schoolCode;
    private String _schoolTitle;
    private String _oldIdPromo;
    private int _oldIdLocation;
    private boolean _invited;
    private int _studentYear;
    private boolean _admin;
    private String _present;
    /** Variables for project parsing */
    private String _grade;
    private String _cycle;
    private String _dateInsc;
    private int _credits;

    /** Variables for modules parsing */
    private String _type;

    public void setType(String _type) { this._type = _type; }

    public String getType() { return _type; }

    public int getCredits() {
        return _credits;
    }

    public void setCredits(int _credits) {
        this._credits = _credits;
    }

    public String getDateInsc() {
        return _dateInsc;
    }

    public void setDateInsc(String _dateInsc) {
        this._dateInsc = _dateInsc;
    }

    public String getCycle() {
        return _cycle;
    }

    public void setCycle(String _cycle) {
        this._cycle = _cycle;
    }

    public String getGrade() {
        return _grade;
    }

    public void setGrade(String _grade) {
        this._grade = _grade;
    }

    public int getId() {
        return _id;
    }

    public void setId(int _id) {
        this._id = _id;
    }

    public String getLogin() {
        return _login;
    }

    public void setLogin(String _login) {
        this._login = _login;
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String _title) {
        this._title = _title;
    }

    public String getEmail() {
        return _email;
    }

    public void setEmail(String _email) {
        this._email = _email;
    }

    public String getInternalEmail() {
        return _internalEmail;
    }

    public void setInternalEmail(String _internalEmail) {
        this._internalEmail = _internalEmail;
    }

    public String getLastName() {
        return _lastName;
    }

    public void setLastName(String _lastName) {
        this._lastName = _lastName;
    }

    public String getFirstName() {
        return _firstName;
    }

    public void setFirstName(String _firstName) {
        this._firstName = _firstName;
    }

    public boolean getReferentUsed() {
        return _referentUsed;
    }

    public void setReferentUsed(boolean _referentUsed) {
        this._referentUsed = _referentUsed;
    }

    public String getPicture() {
        return _picture;
    }

    public void setPicture(String _picture) {
        this._picture = _picture;
    }

    public String getPictureFun() {
        return _pictureFun;
    }

    public void setPictureFun(String _pictureFun) {
        this._pictureFun = _pictureFun;
    }

    public String getEmailReferent() {
        return _emailReferent;
    }

    public void setEmailReferent(String _emailReferent) {
        this._emailReferent = _emailReferent;
    }

    public String getPassReferent() {
        return _passReferent;
    }

    public void setPassReferent(String _passReferent) {
        this._passReferent = _passReferent;
    }

    public int getPromo() {
        return _promo;
    }

    public void setPromo(int _promo) {
        this._promo = _promo;
    }

    public String getSemester() {
        return _semester;
    }

    public void setSemester(String _semester) {
        this._semester = _semester;
    }

    public int getUid() {
        return _uid;
    }

    public void setUid(int _uid) {
        this._uid = _uid;
    }

    public int getGid() {
        return _gid;
    }

    public void setGid(int _gid) {
        this._gid = _gid;
    }

    public String getLocation() {
        return _location;
    }

    public void setLocation(String _location) {
        this._location = _location;
    }

    public String getDocuments() {
        return _documents;
    }

    public void setDocuments(String _documents) {
        this._documents = _documents;
    }

    public String getUserdocs() {
        return _userdocs;
    }

    public void setUserdocs(String _userdocs) {
        this._userdocs = _userdocs;
    }

    public String getShell() {
        return _shell;
    }

    public void setShell(String _shell) {
        this._shell = _shell;
    }

    public String getNetsoul() {
        return _netsoul;
    }

    public void setNetsoul(String _netsoul) {
        this._netsoul = _netsoul;
    }

    public boolean setclose() {
        return _close;
    }

    public void setClose(boolean _close) {
        this._close = _close;
    }

    public String getCloseReason() {
        return _closeReason;
    }

    public void setCloseReason(String _closeReason) {
        this._closeReason = _closeReason;
    }

    public String getCtime() {
        return _ctime;
    }

    public void setCtime(String _ctime) {
        this._ctime = _ctime;
    }

    public String getMtime() {
        return _mtime;
    }

    public void setMtime(String _mtime) {
        this._mtime = _mtime;
    }

    public String getComment() {
        return _comment;
    }

    public void setComment(String _comment) {
        this._comment = _comment;
    }

    public int getIdPromo() {
        return _idPromo;
    }

    public void setIdPromo(int _idPromo) {
        this._idPromo = _idPromo;
    }

    public int getIdHistory() {
        return _idHistory;
    }

    public void setIdHistory(int _idHistory) {
        this._idHistory = _idHistory;
    }

    public String getCourseCode() {
        return _courseCode;
    }

    public void setCourseCode(String _courseCode) {
        this._courseCode = _courseCode;
    }

    public String getSchoolCode() {
        return _schoolCode;
    }

    public void setSchoolCode(String _schoolCode) {
        this._schoolCode = _schoolCode;
    }

    public String getSchoolTitle() {
        return _schoolTitle;
    }

    public void setSchoolTitle(String _schoolTitle) {
        this._schoolTitle = _schoolTitle;
    }

    public String getOldIdPromo() {
        return _oldIdPromo;
    }

    public void setOldIdPromo(String _oldIdPromo) {
        this._oldIdPromo = _oldIdPromo;
    }

    public int getOldIdLocation() {
        return _oldIdLocation;
    }

    public void setOldIdLocation(int _oldIdLocation) {
        this._oldIdLocation = _oldIdLocation;
    }

    public boolean getInvited() {
        return _invited;
    }

    public void setInvited(boolean _invited) {
        this._invited = _invited;
    }

    public int getStudentYear() {
        return _studentYear;
    }

    public void setStudentYear(int _studentYear) {
        this._studentYear = _studentYear;
    }

    public boolean getAdmin() {
        return _admin;
    }

    public void setAdmin(boolean _admin) {
        this._admin = _admin;
    }

    public String getIp() {
        return _ip;
    }

    public void setIp(String _ip) {
        this._ip = _ip;
    }

    public String getActiveLog() {
        return _activeLog;
    }

    public void setActiveLog(String _activeLog) {
        this._activeLog = _activeLog;
    }

    public int getCreditsMin() {
        return _creditsMin;
    }

    public void setCreditsMin(int _creditsMin) {
        this._creditsMin = _creditsMin;
    }

    public int getCreditsNorm() {
        return _creditsNorm;
    }

    public void setCreditsNorm(int _creditsNorm) {
        this._creditsNorm = _creditsNorm;
    }

    public int getCreditsObj() {
        return _creditsObj;
    }

    public void setCreditsObj(int _creditsObj) {
        this._creditsObj = _creditsObj;
    }

    public int getNslogMin() {
        return _nslogMin;
    }

    public void setNslogMin(int _nslogMin) {
        this._nslogMin = _nslogMin;
    }

    public int getNslogNorm() {
        return _nslogNorm;
    }

    public void setNslogNorm(int _nslogNorm) {
        this._nslogNorm = _nslogNorm;
    }

    public String getSemesterCode() {
        return _semesterCode;
    }

    public void setSemesterCode(String _semesterCode) {
        this._semesterCode = _semesterCode;
    }

    public int getSemesterNum() {
        return _semesterNum;
    }

    public void setSemesterNum(int _semesterNum) {
        this._semesterNum = _semesterNum;
    }

    public int getAchieved() {
        return _achieved;
    }

    public void setAchieved(int _achieved) {
        this._achieved = _achieved;
    }

    public int getFailed() {
        return _failed;
    }

    public void setFailed(int _failed) {
        this._failed = _failed;
    }

    public int getInProgress() {
        return _inProgress;
    }

    public void setInProgress(int _inProgress) {
        this._inProgress = _inProgress;
    }

    public void setPresent(String present) {
        this._present = present;
    }

    public String getPresent() {return _present;}

    static public Utilisateur  parse(String json) throws JSONException {
        Utilisateur utilisateur = new Utilisateur();
        JSONObject object = null;
        object = new JSONObject(json);
        if (object != null)
        {

            /** Parsing des ints */
            if (object.has("id"))
                utilisateur.setId(object.getInt("id"));
            if (object.has("promo"))
                utilisateur.setPromo(object.getInt("promo"));
            if (object.has("uid"))
                utilisateur.setUid(object.getInt("uid"));
            if (object.has("gid"))
                utilisateur.setGid(object.getInt("gid"));
            if (object.has("id_promo"))
                utilisateur.setIdPromo(object.getInt("id_promo"));
            if (object.has("id_history"))
                utilisateur.setIdHistory(object.getInt("id_history"));
            if (object.has("old_id_location"))
                utilisateur.setOldIdLocation(object.getInt("old_id_location"));
            if (object.has("studentyear"))
                utilisateur.setStudentYear(object.getInt("studentyear"));
            if (object.has("credits"))
                utilisateur.setCredits(object.getInt("credits"));


            /** Parsing des strings */
            if (object.has("login"))
                utilisateur.setLogin(object.getString("login"));
            if (object.has("type"))
                utilisateur.setLogin(object.getString("type"));
            if (object.has("title"))
                utilisateur.setTitle(object.getString("title"));
            if (object.has("email"))
                utilisateur.setEmail(object.getString("email"));
            if (object.has("internal_email"))
                utilisateur.setInternalEmail(object.getString("internal_email"));
            if (object.has("lastname"))
                utilisateur.setLastName(object.getString("lastname"));
            if (object.has("firstname"))
                utilisateur.setFirstName(object.getString("firstname"));
            if (object.has("picture"))
                utilisateur.setPicture(object.getString("picture"));
            if (object.has("picture_fun"))
                utilisateur.setPictureFun(object.getString("picture_fun"));
            if (object.has("email_referent"))
                utilisateur.setEmailReferent(object.getString("email_referent"));
            if (object.has("pass_referent"))
                utilisateur.setPassReferent(object.getString("pass_referent"));
            if (object.has("semester"))
                utilisateur.setSemester(object.getString("semester"));
            if (object.has("location"))
                utilisateur.setLocation(object.getString("location"));
            if (object.has("documents"))
                utilisateur.setDocuments(object.getString("documents"));
            if (object.has("userdocs"))
                utilisateur.setUserdocs(object.getString("userdocs"));
            if (object.has("shell"))
                utilisateur.setShell(object.getString("shell"));
            if (object.has("netsoul"))
                utilisateur.setNetsoul(object.getString("netsoul"));
            if (object.has("close_reason"))
                utilisateur.setCloseReason(object.getString("close_reason"));
            if (object.has("ctime"))
                utilisateur.setCtime(object.getString("ctime"));
            if (object.has("mtime"))
                utilisateur.setMtime(object.getString("mtime"));
            if (object.has("comment"))
                utilisateur.setComment(object.getString("comment"));
            if (object.has("course_code"))
                utilisateur.setCourseCode(object.getString("course_code"));
            if (object.has("school_code"))
                utilisateur.setSchoolCode(object.getString("school_code"));
            if (object.has("school_title"))
                utilisateur.setSchoolTitle(object.getString("school_title"));
            if (object.has("old_id_promo"))
                utilisateur.setOldIdPromo(object.getString("old_id_promo"));
            if (object.has("grade"))
                utilisateur.setGrade(object.getString("grade"));
            if (object.has("cycle"))
                utilisateur.setCycle(object.getString("cycle"));
            if (object.has("date_ins"))
                utilisateur.setDateInsc(object.getString("date_ins"));
            if (object.has("present"))
                utilisateur.setPresent(object.getString("present"));

            /** Parsing des booléens */
            if (object.has("referent_used"))
                utilisateur.setReferentUsed(object.getBoolean("referent_used"));
            if (object.has("close"))
                utilisateur.setReferentUsed(object.getBoolean("close"));
            if (object.has("invited"))
                utilisateur.setReferentUsed(object.getBoolean("invited"));
            if (object.has("admin"))
                utilisateur.setReferentUsed(object.getBoolean("admin"));
        }
        else
            return null;
        return utilisateur;
    }

    static public List<Utilisateur> parseTrombi(String json) throws JSONException {
        JSONObject obj = new JSONObject(json);
        JSONArray array = obj.getJSONArray("items");
        return Utilisateur.parses(array.toString());
    }

    static public List<Utilisateur> parses(String json) throws JSONException {
        List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
        JSONArray object = null;
        object = new JSONArray(json);
        int numberOfItems = object.length();
        for (int i = 0; i != numberOfItems; i++) {
            JSONObject jsonobj = object.getJSONObject(i);
            Utilisateur utilisateur = new Utilisateur();

            /** Parsing des ints */
            if (jsonobj.has("id"))
                utilisateur.setId(jsonobj.getInt("id"));
            if (jsonobj.has("promo"))
                utilisateur.setPromo(jsonobj.getInt("promo"));
            if (jsonobj.has("uid"))
                utilisateur.setUid(jsonobj.getInt("uid"));
            if (jsonobj.has("gid"))
                utilisateur.setGid(jsonobj.getInt("gid"));
            if (jsonobj.has("id_promo"))
                utilisateur.setIdPromo(jsonobj.getInt("id_promo"));
            if (jsonobj.has("id_history"))
                utilisateur.setIdHistory(jsonobj.getInt("id_history"));
            if (jsonobj.has("old_id_location"))
                utilisateur.setOldIdLocation(jsonobj.getInt("old_id_location"));
            if (jsonobj.has("studentyear"))
                utilisateur.setStudentYear(jsonobj.getInt("studentyear"));
            if (jsonobj.has("credits"))
                utilisateur.setCredits(jsonobj.getInt("credits"));

            /** Parsing des strings */
            if (jsonobj.has("login"))
                utilisateur.setLogin(jsonobj.getString("login"));
            if (jsonobj.has("type"))
                utilisateur.setLogin(jsonobj.getString("type"));
            if (jsonobj.has("title"))
                utilisateur.setTitle(jsonobj.getString("title"));
            if (jsonobj.has("email"))
                utilisateur.setEmail(jsonobj.getString("email"));
            if (jsonobj.has("internal_email"))
                utilisateur.setInternalEmail(jsonobj.getString("internal_email"));
            if (jsonobj.has("lastname"))
                utilisateur.setLastName(jsonobj.getString("lastname"));
            if (jsonobj.has("firstname"))
                utilisateur.setFirstName(jsonobj.getString("firstname"));
            if (jsonobj.has("picture"))
                utilisateur.setPicture(jsonobj.getString("picture"));
            if (jsonobj.has("picture_fun"))
                utilisateur.setPictureFun(jsonobj.getString("picture_fun"));
            if (jsonobj.has("email_referent"))
                utilisateur.setEmailReferent(jsonobj.getString("email_referent"));
            if (jsonobj.has("pass_referent"))
                utilisateur.setPassReferent(jsonobj.getString("pass_referent"));
            if (jsonobj.has("semester"))
                utilisateur.setSemester(jsonobj.getString("semester"));
            if (jsonobj.has("location"))
                utilisateur.setLocation(jsonobj.getString("location"));
            if (jsonobj.has("documents"))
                utilisateur.setDocuments(jsonobj.getString("documents"));
            if (jsonobj.has("userdocs"))
                utilisateur.setUserdocs(jsonobj.getString("userdocs"));
            if (jsonobj.has("shell"))
                utilisateur.setShell(jsonobj.getString("shell"));
            if (jsonobj.has("netsoul"))
                utilisateur.setNetsoul(jsonobj.getString("netsoul"));
            if (jsonobj.has("close_reason"))
                utilisateur.setCloseReason(jsonobj.getString("close_reason"));
            if (jsonobj.has("ctime"))
                utilisateur.setCtime(jsonobj.getString("ctime"));
            if (jsonobj.has("mtime"))
                utilisateur.setMtime(jsonobj.getString("mtime"));
            if (jsonobj.has("comment"))
                utilisateur.setComment(jsonobj.getString("comment"));
            if (jsonobj.has("course_code"))
                utilisateur.setCourseCode(jsonobj.getString("course_code"));
            if (jsonobj.has("school_code"))
                utilisateur.setSchoolCode(jsonobj.getString("school_code"));
            if (jsonobj.has("school_title"))
                utilisateur.setSchoolTitle(jsonobj.getString("school_title"));
            if (jsonobj.has("old_id_promo"))
                utilisateur.setOldIdPromo(jsonobj.getString("old_id_promo"));
            if (jsonobj.has("grade"))
                utilisateur.setGrade(jsonobj.getString("grade"));
            if (jsonobj.has("cycle"))
                utilisateur.setCycle(jsonobj.getString("cycle"));
            if (jsonobj.has("date_ins"))
                utilisateur.setDateInsc(jsonobj.getString("date_ins"));

            /** Parsing des booléens */
            if (jsonobj.has("referent_used"))
                utilisateur.setReferentUsed(jsonobj.getBoolean("referent_used"));
            if (jsonobj.has("close"))
                utilisateur.setReferentUsed(jsonobj.getBoolean("close"));
            if (jsonobj.has("invited"))
                utilisateur.setReferentUsed(jsonobj.getBoolean("invited"));
            if (jsonobj.has("admin"))
                utilisateur.setReferentUsed(jsonobj.getBoolean("admin"));
            utilisateurs.add(utilisateur);
        }
        return utilisateurs;
    }
}
