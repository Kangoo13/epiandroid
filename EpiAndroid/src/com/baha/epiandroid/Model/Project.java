package com.baha.epiandroid.Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KangooRou on 19/01/2015.
 */
public class Project {

    private String _instanceLocation;
    private int _idActivite;
    private String _typeTitle;
    private String _typeCode;
    private boolean _register;
    private int _registerByBloc;
    private int _registerProf;
    private int _nbMin;
    private int _nbMax;
    private String _endRegister;
    private String _deadline;
    private boolean _isRdv;
    private int _instanceAllowed;
    private String _title;
    private String _description;
    private boolean _closed;
    private int _over;
    private String _overDeadline;
    private boolean _dateAccess;
    private int _instanceRegistered;
    private String _userProjectStatus;
    private int _note;
    private String _rootSlug;
    private String _forumPath;
    private String _slug;
    private int _callIhk;
    private int _nbNotes;
    private String _userProjectMaster;
    private String _userProjectCode;
    private String _userProjectTitle;
    private int _registeredInstance;
    private List<Utilisateur> _notRegisteredUsers;
    private List<Utilisateur> _registeredUsers;
    private String _codeModule;
    private String _project;
    private String _endActi;
    private String _actiTitle;
    private String _numEvent;
    private String _seats;
    private String _titleModule;
    private String _beginEvent;
    private String _rights;
    private int _num;
    private String _beginActi;
    private int _scolarYear;
    private String _codeLocation;
    private String _endEvent;
    private String _typeActiCode;
    private String _codeActi;
    private String _infoCreneau;
    private int _registered;
    private String _codeInstance;
    private String _typeActi;

    /** Variable for Module Parsing */
    private String _start;
    private int _nbHours;
    private int _nbGroup;
    private String _titleLocationType;
    private boolean _isProjet;
    private int _idProjet;
    private boolean _isNote;
    private boolean _isBlocins;
    private String _rdvStatus;
    private int _idBareme;
    private String _titleBareme;
    private int _archive;
    private String _hashElearning;
    private String _gedNodeAdm;
    private int _nbPlanified;

    public int getNbPlanified() {
        return _nbPlanified;
    }

    public void setNbPlanified(int _nbPlanified) {
        this._nbPlanified = _nbPlanified;
    }

    public String getGedNodeAdm() {
        return _gedNodeAdm;
    }

    public void setGedNodeAdm(String _gedNodeAdm) {
        this._gedNodeAdm = _gedNodeAdm;
    }

    public String getHashElearning() {
        return _hashElearning;
    }

    public void setHashElearning(String _hashElearning) {
        this._hashElearning = _hashElearning;
    }

    public int getArchive() {
        return _archive;
    }

    public void setArchive(int _archive) {
        this._archive = _archive;
    }

    public String getTitleBareme() {
        return _titleBareme;
    }

    public void setTitleBareme(String _titleBareme) {
        this._titleBareme = _titleBareme;
    }

    public int getIdBareme() {
        return _idBareme;
    }

    public void setIdBareme(int _idBareme) {
        this._idBareme = _idBareme;
    }

    public String getRdvStatus() {
        return _rdvStatus;
    }

    public void setRdvStatus(String _rdvStatus) {
        this._rdvStatus = _rdvStatus;
    }

    public boolean getIsBlocins() {
        return _isBlocins;
    }

    public void setIsBlocins(boolean _isBlocins) {
        this._isBlocins = _isBlocins;
    }

    public boolean getIsNote() {
        return _isNote;
    }

    public void setIsNote(boolean _isNote) {
        this._isNote = _isNote;
    }

    public int getIdProjet() {
        return _idProjet;
    }

    public void setIdProjet(int _idProjet) {
        this._idProjet = _idProjet;
    }

    public boolean getIsProjet() {
        return _isProjet;
    }

    public void setIsProjet(boolean _isProjet) {
        this._isProjet = _isProjet;
    }

    public String getTitleLocationType() {
        return _titleLocationType;
    }

    public void setTitleLocationType(String _titleLocationType) {
        this._titleLocationType = _titleLocationType;
    }

    public int getNbGroup() {
        return _nbGroup;
    }

    public void setNbGroup(int _nbGroup) {
        this._nbGroup = _nbGroup;
    }

    public int getNbHours() {
        return _nbHours;
    }

    public void setNbHours(int _nbHours) {
        this._nbHours = _nbHours;
    }

    public String getStart() {
        return _start;
    }

    public void setStart(String _start) {
        this._start = _start;
    }

    public List<Utilisateur> getRegisteredUsers() {
        return _registeredUsers;
    }

    public void setRegisteredUsers(List<Utilisateur> _registeredUsers) {
        this._registeredUsers = _registeredUsers;
    }

    public String getInstanceLocation() {
        return _instanceLocation;
    }

    public void setInstanceLocation(String _instanceLocation) {
        this._instanceLocation = _instanceLocation;
    }

    public int getIdActivite() {
        return _idActivite;
    }

    public void setIdActivite(int _idActivite) {
        this._idActivite = _idActivite;
    }

    public String getTypeTitle() {
        return _typeTitle;
    }

    public void setTypeTitle(String _typeTitle) {
        this._typeTitle = _typeTitle;
    }

    public String getTypeCode() {
        return _typeCode;
    }

    public void setTypeCode(String _typeCode) {
        this._typeCode = _typeCode;
    }

    public boolean is_register() {
        return _register;
    }

    public void setRegister(boolean _register) {
        this._register = _register;
    }

    public int getRegisterByBloc() {
        return _registerByBloc;
    }

    public void setRegisterByBloc(int _registerByBloc) {
        this._registerByBloc = _registerByBloc;
    }

    public int getRegisterProf() {
        return _registerProf;
    }

    public void setRegisterProf(int _registerProf) {
        this._registerProf = _registerProf;
    }

    public int getNbMin() {
        return _nbMin;
    }

    public void setNbMin(int _nbMin) {
        this._nbMin = _nbMin;
    }

    public int getNbMax() {
        return _nbMax;
    }

    public void setNbMax(int _nbMax) {
        this._nbMax = _nbMax;
    }

    public String getEndRegister() {
        return _endRegister;
    }

    public void setEndRegister(String _endRegister) {
        this._endRegister = _endRegister;
    }

    public String getDeadline() {
        return _deadline;
    }

    public void setDeadline(String _deadline) {
        this._deadline = _deadline;
    }

    public boolean is_isRdv() {
        return _isRdv;
    }

    public void setIsRdv(boolean _isRdv) {
        this._isRdv = _isRdv;
    }

    public int getInstanceAllowed() {
        return _instanceAllowed;
    }

    public void setInstanceAllowed(int _instanceAllowed) {
        this._instanceAllowed = _instanceAllowed;
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String _title) {
        this._title = _title;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String _description) {
        this._description = _description;
    }

    public boolean is_closed() {
        return _closed;
    }

    public void setClosed(boolean _closed) {
        this._closed = _closed;
    }

    public int getOver() {
        return _over;
    }

    public void setOver(int _over) {
        this._over = _over;
    }

    public String getOverDeadline() {
        return _overDeadline;
    }

    public void setOverDeadline(String _overDeadline) {
        this._overDeadline = _overDeadline;
    }

    public boolean is_dateAccess() {
        return _dateAccess;
    }

    public void setDateAccess(boolean _dateAccess) {
        this._dateAccess = _dateAccess;
    }

    public int getInstanceRegistered() {
        return _instanceRegistered;
    }

    public void setInstanceRegistered(int _instanceRegistered) {
        this._instanceRegistered = _instanceRegistered;
    }

    public String getUserProjectStatus() {
        return _userProjectStatus;
    }

    public void setUserProjectStatus(String _userProjectStatus) {
        this._userProjectStatus = _userProjectStatus;
    }

    public int getNote() {
        return _note;
    }

    public void setNote(int _note) {
        this._note = _note;
    }

    public String getRootSlug() {
        return _rootSlug;
    }

    public void setRootSlug(String _rootSlug) {
        this._rootSlug = _rootSlug;
    }

    public String getForumPath() {
        return _forumPath;
    }

    public void setForumPath(String _forumPath) {
        this._forumPath = _forumPath;
    }

    public String getSlug() {
        return _slug;
    }

    public void setSlug(String _slug) {
        this._slug = _slug;
    }

    public int getCallIhk() {
        return _callIhk;
    }

    public void setCallIhk(int _callIhk) {
        this._callIhk = _callIhk;
    }

    public int getNbNotes() {
        return _nbNotes;
    }

    public void setNbNotes(int _nbNotes) {
        this._nbNotes = _nbNotes;
    }

    public String getUserProjectMaster() {
        return _userProjectMaster;
    }

    public void setUserProjectMaster(String _userProjectMaster) {
        this._userProjectMaster = _userProjectMaster;
    }

    public String getUserProjectCode() {
        return _userProjectCode;
    }

    public void setUserProjectCode(String _userProjectCode) {
        this._userProjectCode = _userProjectCode;
    }

    public String getUserProjectTitle() {
        return _userProjectTitle;
    }

    public void setUserProjectTitle(String _userProjectTitle) {
        this._userProjectTitle = _userProjectTitle;
    }

    public int getRegisteredInstance() {
        return _registeredInstance;
    }

    public void setRegisteredInstance(int _registeredInstance) {
        this._registeredInstance = _registeredInstance;
    }

    public List<Utilisateur> getNotRegisteredUsers() {
        return _notRegisteredUsers;
    }

    public void setNotRegisteredUsers(List<Utilisateur> _notRegisteredUsers) {
        this._notRegisteredUsers = _notRegisteredUsers;
    }

    public String getCodeModule() {
        return _codeModule;
    }

    public void setCodeModule(String _codeModule) {
        this._codeModule = _codeModule;
    }

    public String getProject() {
        return _project;
    }

    public void setProject(String _project) {
        this._project = _project;
    }

    public String getEndActi() {
        return _endActi;
    }

    public void setEndActi(String _endActi) {
        this._endActi = _endActi;
    }

    public String getActiTitle() {
        return _actiTitle;
    }

    public void setActiTitle(String _actiTitle) {
        this._actiTitle = _actiTitle;
    }

    public String getNumEvent() {
        return _numEvent;
    }

    public void setNumEvent(String _numEvent) {
        this._numEvent = _numEvent;
    }

    public String getSeats() {
        return _seats;
    }

    public void setSeats(String _seats) {
        this._seats = _seats;
    }

    public String getTitleModule() {
        return _titleModule;
    }

    public void setTitleModule(String _titleModule) {
        this._titleModule = _titleModule;
    }

    public String getBeginEvent() {
        return _beginEvent;
    }

    public void setBeginEvent(String _beginEvent) {
        this._beginEvent = _beginEvent;
    }

    public String getRights() {
        return _rights;
    }

    public void setRights(String _rights) {
        this._rights = _rights;
    }

    public int getNum() {
        return _num;
    }

    public void setNum(int _num) {
        this._num = _num;
    }

    public String getBeginActi() {
        return _beginActi;
    }

    public void setBeginActi(String _beginActi) {
        this._beginActi = _beginActi;
    }

    public int getScolarYear() {
        return _scolarYear;
    }

    public void setScolarYear(int _scolarYear) {
        this._scolarYear = _scolarYear;
    }

    public String getCodeLocation() {
        return _codeLocation;
    }

    public void setCodeLocation(String _codeLocation) {
        this._codeLocation = _codeLocation;
    }

    public String getEndEvent() {
        return _endEvent;
    }

    public void setEndEvent(String _endEvent) {
        this._endEvent = _endEvent;
    }

    public String getTypeActiCode() {
        return _typeActiCode;
    }

    public void setTypeActiCode(String _typeActiCode) {
        this._typeActiCode = _typeActiCode;
    }

    public String getCodeActi() {
        return _codeActi;
    }

    public void setCodeActi(String _codeActi) {
        this._codeActi = _codeActi;
    }

    public String getInfoCreneau() {
        return _infoCreneau;
    }

    public void setInfoCreneau(String _infoCreneau) {
        this._infoCreneau = _infoCreneau;
    }

    public int getRegistered() {
        return _registered;
    }

    public void setRegistered(int _registered) {
        this._registered = _registered;
    }

    public String getCodeInstance() {
        return _codeInstance;
    }

    public void setCodeInstance(String _codeInstance) {
        this._codeInstance = _codeInstance;
    }

    public String getTypeActi() {
        return _typeActi;
    }

    public void setTypeActi(String _typeActi) {
        this._typeActi = _typeActi;
    }


    static public List<Project>  parses(String json) throws JSONException {
        List<Project> projects = new ArrayList<Project>();
        JSONArray object = null;
        object = new JSONArray(json);
        if (object != null)
        {
            JSONArray rightsarray = null;
            JSONObject obj = null;
            final int numberOfItems = object.length();
            for (int i = 0; i != numberOfItems; i++) {
                obj = object.getJSONObject(i);
                Project project = new Project();

                project.setCodeModule(obj.getString("codemodule"));
                project.setProject(obj.getString("project"));
                project.setEndActi(obj.getString("end_acti"));
                project.setActiTitle(obj.getString("acti_title"));
                project.setNumEvent(obj.getString("num_event"));
                project.setSeats(obj.getString("seats"));
                project.setTitleModule(obj.getString("title_module"));
                project.setBeginEvent(obj.getString("begin_event"));
                project.setBeginActi(obj.getString("begin_acti"));
                project.setCodeLocation(obj.getString("code_location"));
                project.setEndEvent(obj.getString("end_event"));
                project.setTypeActiCode(obj.getString("type_acti_code"));
                project.setCodeActi(obj.getString("codeacti"));
                project.setInfoCreneau(obj.getString("info_creneau"));
                project.setCodeInstance(obj.getString("codeinstance"));
                project.setTypeActi(obj.getString("type_acti"));

                project.setNum(obj.getInt("num"));
                project.setScolarYear(obj.getInt("scolaryear"));
                project.setRegistered(obj.getInt("registered"));

                if (obj.has("start"))
                    project.setStart(obj.getString("start"));
                if (obj.has("nb_hours"))
                    project.setNbHours(obj.getInt("nb_hours"));
                if (obj.has("nb_group"))
                    project.setNbHours(obj.getInt("nb_group"));
                if (obj.has("title_location_type"))
                    project.setTitleLocationType(obj.getString("title_location_type"));
                if (obj.has("is_projet"))
                    project.setIsProjet(obj.getBoolean("is_projet"));
                if (obj.has("id_projet"))
                    project.setIsProjet(obj.getBoolean("id_projet"));
                if (obj.has("is_note"))
                    project.setIsNote(obj.getBoolean("is_note"));
                if (obj.has("is_blocins"))
                    project.setIsBlocins(obj.getBoolean("is_blocins"));
                if (obj.has("rdv_status"))
                    project.setRdvStatus(obj.getString("rdv_status"));
                if (obj.has("id_bareme"))
                    project.setIdBareme(obj.getInt("id_bareme"));
                if (obj.has("title_bareme"))
                    project.setTitleBareme(obj.getString("title_bareme"));
                if (obj.has("archive"))
                    project.setArchive(obj.getInt("archive"));
                if (obj.has("hash_elearning"))
                    project.setHashElearning(obj.getString("hash_elearning"));
                if (obj.has("ged_node_adm"))
                    project.setGedNodeAdm(obj.getString("ged_node_adm"));
                if (obj.has("nb_planified"))
                    project.setNbPlanified(obj.getInt("nb_planified"));

                rightsarray = obj.getJSONArray("rights");
                project.setRights(rightsarray.getString(0));
                projects.add(project);
            }

        }
        return projects;
    }

    static public Project parse(String json) throws JSONException {
        Project project = new Project();
        JSONObject object = null;
        int numberOfItems;
        object = new JSONObject(json);
        project.setScolarYear(object.getInt("scolaryear"));
        project.setIdActivite(object.getInt("id_activite"));
        project.setRegisterByBloc(object.getInt("register_by_bloc"));
        project.setRegisterProf(object.getInt("register_prof"));
        project.setNbMin(object.getInt("nb_min"));
        project.setNbMax(object.getInt("nb_max"));
        project.setInstanceAllowed(object.getInt("instance_allowed"));
        project.setOver(object.getInt("over"));
        project.setInstanceRegistered(object.getInt("instance_registered"));
        project.setNote(object.getInt("note"));
        project.setCallIhk(object.getInt("call_ihk"));
        project.setNbNotes(object.getInt("nb_notes"));
        project.setRegisteredInstance(object.getInt("registered_instance"));

        project.setCodeModule(object.getString("codemodule"));
        project.setCodeInstance(object.getString("codeinstance"));
        project.setCodeActi(object.getString("codeacti"));
        project.setInstanceLocation(object.getString("instance_location"));
        project.setTitleModule(object.getString("module_title"));
        project.setProject(object.getString("project_title"));
        project.setTypeTitle(object.getString("type_title"));
        project.setTypeCode(object.getString("type_code"));
        project.setBeginActi(object.getString("begin"));
        project.setEndActi(object.getString("end"));
        project.setEndRegister(object.getString("end_register"));
        project.setDeadline(object.getString("deadline"));
        project.setTitle(object.getString("title"));
        project.setDescription(object.getString("description"));
        project.setOverDeadline(object.getString("over_deadline"));
        project.setUserProjectStatus(object.getString("user_project_status"));
        project.setRootSlug(object.getString("root_slug"));
        project.setForumPath(object.getString("forum_path"));
        project.setSlug(object.getString("slug"));
        project.setUserProjectMaster(object.getString("user_project_master"));
        project.setUserProjectCode(object.getString("user_project_code"));
        project.setUserProjectTitle(object.getString("user_project_title"));

        project.setRegister(object.getBoolean("register"));
        project.setIsRdv(object.getBoolean("is_rdv"));
        project.setClosed(object.getBoolean("closed"));
        project.setDateAccess(object.getBoolean("date_access"));

        String registered = object.getJSONArray("registered").toString();
        String notregistered = object.getJSONArray("not registered").toString();

        project.setRegisteredUsers(Utilisateur.parses(registered));
        project.setNotRegisteredUsers(Utilisateur.parses(notregistered));



        return project;
    }
}
