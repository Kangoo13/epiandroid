package com.baha.epiandroid.Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yosh on 19/01/2015.
 */
public class Module {
    public List<Utilisateur> getResp() {
        return _resp;
    }

    public void setResp(List<Utilisateur> _resp) {
        this._resp = _resp;
    }

    private List<Utilisateur> _resp;
    private List<Utilisateur> _respTemplate;
    private List<Utilisateur> _assistant;
    private List<Project> _activites;
    private int scolarYear;
    private String idUserHistory;
    private String codeModule;
    private String codeInstance;
    private String title;
    private String dateIns;
    private int flags;
    private int idInstance;
    private String cycle;
    private String grade;
    private int credits;
    private int barrage;
    private int instanceId;
    private double moduleRating;
    private int semester;
    private String description;
    private String competence;
    private String begin;
    private String endRegister;
    private String end;
    private String userCredits;
    private int opened;
    private int closed;
    private int past;
    private int instanceFlags;
    private String maxIns;
    private String oldAclBackUp;
    private String instanceLocation;
    private String rights;
    private int allowRegister;
    private int studentRegistered;
    private int studentCredits;
    private String studentGrade;
    private String color;
    private String studentFlags;
    private boolean currentResp;

    public List<Utilisateur> get_resp() {
        return _resp;
    }

    public List<Utilisateur> get_respTemplate() {
        return _respTemplate;
    }

    public void setRespTemplate(List<Utilisateur> _respTemplate) {
        this._respTemplate = _respTemplate;
    }

    public List<Utilisateur> get_assistant() {
        return _assistant;
    }

    public void setAssistant(List<Utilisateur> _assistant) {
        this._assistant = _assistant;
    }

    public List<Project> get_activites() {
        return _activites;
    }

    public void setActivites(List<Project> _activites) {
        this._activites = _activites;
    }

    public boolean isCurrentResp() {
        return currentResp;
    }

    public String getRights() {
        return rights;
    }

    public void setRights(String _rights) {
        this.rights = _rights;
    }

    public int getAllowRegister() {
        return allowRegister;
    }

    public void setAllowRegister(int _allowRegister) {
        this.allowRegister = _allowRegister;
    }

    public int getStudentRegistered() {
        return studentRegistered;
    }

    public void setStudentRegistered(int _studentRegistered) {
        this.studentRegistered = _studentRegistered;
    }

    public int getStudentCredits() {
        return studentCredits;
    }

    public void setStudentCredits(int _studentCredits) {
        this.studentCredits = _studentCredits;
    }

    public String getStudentGrade() {
        return studentGrade;
    }

    public void setStudentGrade(String _studentGrade) {
        this.studentGrade = _studentGrade;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String _color) {
        this.color = _color;
    }

    public String getStudentFlags() {
        return studentFlags;
    }

    public void setStudentFlags(String _studentFlags) {
        this.studentFlags = _studentFlags;
    }

    public boolean getCurrentResp() {
        return currentResp;
    }

    public void setCurrentResp(boolean _currentResp) {
        this.currentResp = _currentResp;
    }

    public String getInstanceLocation() {
        return instanceLocation;
    }

    public void setInstanceLocation(String _instanceLocation) {
        this.instanceLocation = _instanceLocation;
    }

    private int hidden;

    public void setScolarYear(int _scolarYear)
    {
        scolarYear = _scolarYear;
    }

    public void setIdUserHistory(String _idUserHistory)
    {
        idUserHistory = _idUserHistory;
    }

    public void setCodeModule(String _codeModule)  { codeModule = _codeModule; }

    public void setCodeInstance(String _codeInstance)
    {
        codeInstance = _codeInstance;
    }

    public void setTitle(String _title)
    {
        title = _title;
    }

    public void setIdInstance(int _idInstance)
    {
        idInstance = _idInstance;
    }

    public void setDateIns(String _dateIns) { dateIns = _dateIns; }

    public void setFlags(int _flags) { flags = _flags; }

    public void setInstanceId(int _instanceId)
    {
        instanceId = _instanceId;
    }

    public void setCycle(String _cycle)
    {
        cycle = _cycle;
    }

    public void setGrade(String _grade)
    {
        grade = _grade;
    }

    public void setCredits(int _credits)
    {
        credits = _credits;
    }

    public void setBarrage(int _barrage)
    {
        barrage = _barrage;
    }

    public void setModuleRating(double _moduleRating)
    {
        moduleRating = _moduleRating;
    }

    public void setSemester(int _semester)
    {
        semester = _semester;
    }

    public int getScolarYear()
    {
        return scolarYear;
    }

    public String getIdUserHistory()
    {
        return idUserHistory;
    }

    public String getCodeModule()
    {
        return codeModule;
    }

    public String getCodeInstance()
    {
        return codeInstance;
    }

    public String getDateIns() { return dateIns; }

    public int getFlags() { return flags; }

    public String getTitle()
    {
        return title;
    }

    public int getIdInstance()
    {
        return idInstance;
    }

    public int getInstanceId()
    {
        return instanceId;
    }

    public int getCredits()
    {
        return credits;
    }

    public int getBarrage()
    {
        return barrage;
    }

    public String getCycle()
    {
        return cycle;
    }

    public String getGrade()
    {
        return grade;
    }

    double getModuleRating()
    {
        return moduleRating;
    }

    int getSemester()
    {
        return semester;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String _description) {
        this.description = _description;
    }

    public String getCompetence() {
        return competence;
    }

    public void setCompetence(String _competence) {
        this.competence = _competence;
    }

    public String getBegin() {
        return begin;
    }

    public void setBegin(String _begin) {
        this.begin = _begin;
    }

    public String getEndRegister() {
        return endRegister;
    }

    public void setEndRegister(String _endRegister) {
        this.endRegister = _endRegister;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String _end) {
        this.end = _end;
    }

    public String getUserCredits() {
        return userCredits;
    }

    public void setUserCredits(String _userCredits) {
        this.userCredits = _userCredits;
    }

    public int getOpened() {
        return opened;
    }

    public void setOpened(int _opened) {
        this.opened = _opened;
    }

    public int getClosed() {
        return closed;
    }

    public void setClosed(int _closed) {
        this.closed = _closed;
    }

    public int getPast() {
        return past;
    }

    public void setPast(int _past) {
        this.past = _past;
    }

    public int getInstanceFlags() {
        return instanceFlags;
    }

    public void setInstanceFlags(int _instanceFlags) {
        this.instanceFlags = _instanceFlags;
    }

    public String getMaxIns() {
        return maxIns;
    }

    public void setMaxIns(String _maxIns) {
        this.maxIns = _maxIns;
    }

    public String getOldAclBackUp() {
        return oldAclBackUp;
    }

    public void setOldAclBackUp(String _oldAclBackUp) {
        this.oldAclBackUp = _oldAclBackUp;
    }

    public int getHidden() {
        return hidden;
    }

    public void setHidden(int _hidden) {
        this.hidden = _hidden;
    }

    static public List<Module> parses(String json) throws JSONException {
        List<Module> modules = new ArrayList<Module>();
        JSONObject object = null;
        object = new JSONObject(json);
        if (object != null) {
            JSONArray objectarray = null;
            objectarray = object.getJSONArray("modules");
            JSONObject jsonObj = null;
            final int numberOfItems = objectarray.length();
            System.out.println(numberOfItems);
            for (int i = 0; i != numberOfItems; i++) {
                jsonObj = objectarray.getJSONObject(i);
                Module module = new Module();
                if (jsonObj.has("scolaryear"))
                    module.setScolarYear(jsonObj.getInt("scolaryear"));
                if (jsonObj.has("barrage"))
                    module.setBarrage(jsonObj.getInt("barrage"));
                if (jsonObj.has("codeinstance"))
                    module.setCodeInstance(jsonObj.getString("codeinstance"));
                if (jsonObj.has("codemodule"))
                    module.setCodeModule(jsonObj.getString("codemodule"));
                if (jsonObj.has("credits"))
                    module.setCredits(jsonObj.getInt("credits"));
                if (jsonObj.has("cycle"))
                    module.setCycle(jsonObj.getString("cycle"));
                if (jsonObj.has("grade"))
                    module.setGrade(jsonObj.getString("grade"));
                if (jsonObj.has("id_user_history"))
                    module.setIdUserHistory(jsonObj.getString("id_user_history"));
                if (jsonObj.has("id_instance"))
                    module.setIdInstance(jsonObj.getInt("id_instance"));
                if (jsonObj.has("date_ins"))
                    module.setDateIns(jsonObj.getString("date_ins"));
                if (jsonObj.has("instance_id"))
                    module.setInstanceId(jsonObj.getInt("instance_id"));
                if (jsonObj.has("title"))
                    module.setTitle(jsonObj.getString("title"));
                if (jsonObj.has("flags"))
                    module.setFlags(jsonObj.getInt("flags"));
                modules.add(module);
            }
        }
        return modules;
    }


    static public Module parse(String json) throws JSONException {
        Module module = new Module();
        JSONObject jsonObj = new JSONObject(json);
        JSONArray respObj = null;
        JSONArray respTempObj = null;
        JSONArray assistantObj = null;
        JSONArray activitesObj = null;
        if (jsonObj != null) {
            if (jsonObj.has("scolaryear"))
                module.setScolarYear(jsonObj.getInt("scolaryear"));
            if (jsonObj.has("barrage"))
                module.setBarrage(jsonObj.getInt("barrage"));
            if (jsonObj.has("codeinstance"))
                module.setCodeInstance(jsonObj.getString("codeinstance"));
            if (jsonObj.has("codemodule"))
                module.setCodeModule(jsonObj.getString("codemodule"));
            if (jsonObj.has("credits"))
                module.setCredits(jsonObj.getInt("credits"));
            if (jsonObj.has("cycle"))
                module.setCycle(jsonObj.getString("cycle"));
            if (jsonObj.has("grade"))
                module.setGrade(jsonObj.getString("grade"));
            if (jsonObj.has("id_user_history"))
                module.setIdUserHistory(jsonObj.getString("id_user_history"));
            if (jsonObj.has("id_instance"))
                module.setIdInstance(jsonObj.getInt("id_instance"));
            if (jsonObj.has("instance_id"))
                module.setInstanceId(jsonObj.getInt("instance_id"));
            if (jsonObj.has("title"))
                module.setTitle(jsonObj.getString("title"));
            if (jsonObj.has("flags"))
                module.setFlags(jsonObj.getInt("flags"));
            if (jsonObj.has("description"))
                module.setDescription(jsonObj.getString("description"));
            if (jsonObj.has("competence"))
                module.setCompetence(jsonObj.getString("competence"));
            if (jsonObj.has("begin"))
                module.setBegin(jsonObj.getString("begin"));
            if (jsonObj.has("end_register"))
                module.setEndRegister(jsonObj.getString("end_register"));
            if (jsonObj.has("end"))
                module.setEnd(jsonObj.getString("end"));
            if (jsonObj.has("user_credits"))
                module.setUserCredits(jsonObj.getString("user_credits"));
            if (jsonObj.has("past"))
                module.setPast(jsonObj.getInt("past"));
            if (jsonObj.has("opened"))
                module.setOpened(jsonObj.getInt("opened"));
            if (jsonObj.has("closed"))
                module.setClosed(jsonObj.getInt("closed"));
            if (jsonObj.has("instance_flags"))
                module.setInstanceFlags(jsonObj.getInt("instance_flags"));
            if (jsonObj.has("hidden"))
                module.setHidden(jsonObj.getInt("hidden"));
            if (jsonObj.has("max_ins"))
                module.setMaxIns(jsonObj.getString("max_ins"));
            if (jsonObj.has("instance_location"))
                module.setInstanceLocation(jsonObj.getString("instance_location"));
            if (jsonObj.has("old_acl_backup"))
                module.setOldAclBackUp(jsonObj.getString("old_acl_backup"));
            if (jsonObj.has("rights"))
                module.setRights(jsonObj.getString("rights"));
            if (jsonObj.has("allow_register"))
                module.setAllowRegister(jsonObj.getInt("allow_register"));
            if (jsonObj.has("student_registered"))
                module.setStudentRegistered(jsonObj.getInt("student_registered"));
            if (jsonObj.has("student_grade"))
                module.setStudentGrade(jsonObj.getString("student_grade"));
            if (jsonObj.has("student_credits"))
                module.setStudentCredits(jsonObj.getInt("student_credits"));
            if (jsonObj.has("color"))
                module.setColor(jsonObj.getString("color"));
            if (jsonObj.has("student_flags"))
                module.setStudentFlags(jsonObj.getString("student_flags"));
            if (jsonObj.has("current_resp"))
                module.setCurrentResp(jsonObj.getBoolean("current_resp"));
            respObj = jsonObj.getJSONArray("resp");
            module.setResp(Utilisateur.parses(respObj.toString()));
            respTempObj = jsonObj.getJSONArray("resp_template");
            module.setRespTemplate(Utilisateur.parses(respTempObj.toString()));
            assistantObj = jsonObj.getJSONArray("assistant");
            module.setAssistant(Utilisateur.parses(assistantObj.toString()));
            activitesObj = jsonObj.getJSONArray("activites");
            module.setActivites(Project.parses(activitesObj.toString()));
        }
        return (module);
    }
}
