package com.baha.epiandroid.Model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by KangooRou on 26/01/2015.
 */
public class Informations {

    private Utilisateur _utilisateur;
    private String _ip;
    private String _activeLog;
    private int _creditsMin;
    private int _creditsNorm;
    private int _creditsObj;
    private int _nsLogMin;
    private int _nsLogNorm;
    private String _semesterCode;
    private int _semesterNum;
    private int _achieved;
    private int _failed;
    private int _inProgress;

    public Utilisateur getUtilisateur() {
        return _utilisateur;
    }

    public void setUtilisateur(Utilisateur _utilisateur) {
        this._utilisateur = _utilisateur;
    }

    public int getCreditsObj() {
        return _creditsObj;
    }

    public void setCreditsObj(int _creditsObj) {
        this._creditsObj = _creditsObj;
    }

    public String getIp() {
        return _ip;
    }

    public void setIp(String _ip) {
        this._ip = _ip;
    }

    public String getActiveLog() {
        return _activeLog;
    }

    public void setActiveLog(String _activeLog) {
        this._activeLog = _activeLog;
    }

    public int getCreditsMin() {
        return _creditsMin;
    }

    public void setCreditsMin(int _creditsMin) {
        this._creditsMin = _creditsMin;
    }

    public int getCreditsNorm() {
        return _creditsNorm;
    }

    public void setCreditsNorm(int _creditsNorm) {
        this._creditsNorm = _creditsNorm;
    }

    public int getNsLogMin() {
        return _nsLogMin;
    }

    public void setNsLogMin(int _nsLogMin) {
        this._nsLogMin = _nsLogMin;
    }

    public int getNsLogNorm() {
        return _nsLogNorm;
    }

    public void setNsLogNorm(int _nsLogNorm) {
        this._nsLogNorm = _nsLogNorm;
    }

    public String getSemesterCode() {
        return _semesterCode;
    }

    public void setSemesterCode(String _semesterCode) {
        this._semesterCode = _semesterCode;
    }

    public int getSemesterNum() {
        return _semesterNum;
    }

    public void setSemesterNum(int _semesterNum) {
        this._semesterNum = _semesterNum;
    }

    public int getAchieved() {
        return _achieved;
    }

    public void setAchieved(int _achieved) {
        this._achieved = _achieved;
    }

    public int getFailed() {
        return _failed;
    }

    public void setFailed(int _failed) {
        this._failed = _failed;
    }

    public int getInProgress() {
        return _inProgress;
    }

    public void setInProgress(int _inProgress) {
        this._inProgress = _inProgress;
    }

    static public Informations  parse(String json) throws JSONException {
        Informations information = new Informations();
        JSONObject object = null;
        object = new JSONObject(json);
        if (object != null)
        {
            JSONObject infosobj = null;
            JSONObject currentobj = null;

            if (object.has("ip"))
                information.setIp(object.getString("ip"));

            infosobj = object.getJSONObject("infos");
            information.setUtilisateur(Utilisateur.parse(infosobj.toString()));

            currentobj = object.getJSONObject("current");
            information.setActiveLog(currentobj.getString("active_log"));
            information.setCreditsMin(currentobj.getInt("credits_min"));
            information.setCreditsNorm(currentobj.getInt("credits_norm"));
            information.setCreditsObj(currentobj.getInt("credits_obj"));
            information.setNsLogMin(currentobj.getInt("nslog_min"));
            information.setNsLogNorm(currentobj.getInt("nslog_norm"));
            information.setSemesterNum(currentobj.getInt("semester_num"));
            information.setAchieved(currentobj.getInt("achieved"));
            information.setFailed(currentobj.getInt("failed"));
            information.setInProgress(currentobj.getInt("inprogress"));
            information.setSemesterCode(currentobj.getString("semester_code"));

        }
        return information;
    }
}
