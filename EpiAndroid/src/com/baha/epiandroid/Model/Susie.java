package com.baha.epiandroid.Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KangooRou on 19/01/2015.
 */
public class Susie {
    private List<Utilisateur> _logins;
    private double _ratingStudents;
    private String _startCalendar;
    private String _endCalendar;
    private boolean _registeredCalendar;
    private String _duration;
    private String _start;
    private String _ratingEvent;
    private int _weeksLeft;
    private int _nbPlace;
    private String _color;
    private String _end;
    private int _idCalendar;
    private String _description;
    private String _calendarType;
    private String _location;
    private int _idOwner;
    private int _id;
    private String _loginOwner;
    private String _titleOwner;
    private String _pictureOwner;
    private String _title;
    private boolean _hasToRate;
    private String _eventRegistered;
    private int _idMaker;
    private int _registered;
    private boolean _confirmOwner;
    private int _planningVisibleRight;
    private String _loginMaker;
    private String _titleMaker;
    private boolean _confirmMaker;
    private String _type;
    private String _typeRoom;
    private int _nbRated;

    public List<Utilisateur> getLogins() {
        return _logins;
    }

    public void setLogins(List<Utilisateur> _logins) {
        this._logins = _logins;
    }

    public double getRatingStudents() {
        return _ratingStudents;
    }

    public void setRatingStudents(double _ratingStudents) {
        this._ratingStudents = _ratingStudents;
    }

    public String getStartCalendar() {
        return _startCalendar;
    }

    public void setStartCalendar(String _startCalendar) {
        this._startCalendar = _startCalendar;
    }

    public String getEndCalendar() {
        return _endCalendar;
    }

    public void setEndCalendar(String _endCalendar) {
        this._endCalendar = _endCalendar;
    }

    public boolean getRegisteredCalendar() {
        return _registeredCalendar;
    }

    public void setRegisteredCalendar(boolean _registeredCalendar) {
        this._registeredCalendar = _registeredCalendar;
    }

    public String getDuration() {
        return _duration;
    }

    public void setDuration(String _duration) {
        this._duration = _duration;
    }

    public String getStart() {
        return _start;
    }

    public void setStart(String _start) {
        this._start = _start;
    }

    public String getRatingEvent() {
        return _ratingEvent;
    }

    public void setRatingEvent(String _ratingEvent) {
        this._ratingEvent = _ratingEvent;
    }

    public int getWeeksLeft() {
        return _weeksLeft;
    }

    public void setWeeksLeft(int _weeksLeft) {
        this._weeksLeft = _weeksLeft;
    }

    public int getNbPlace() {
        return _nbPlace;
    }

    public void setNbPlace(int _nbPlace) {
        this._nbPlace = _nbPlace;
    }

    public String getColor() {
        return _color;
    }

    public void setColor(String _color) {
        this._color = _color;
    }

    public String getEnd() {
        return _end;
    }

    public void setEnd(String _end) {
        this._end = _end;
    }

    public int getIdCalendar() {
        return _idCalendar;
    }

    public void setIdCalendar(int _idCalendar) {
        this._idCalendar = _idCalendar;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String _description) {
        this._description = _description;
    }

    public String getCalendarType() {
        return _calendarType;
    }

    public void setCalendarType(String _calendarType) {
        this._calendarType = _calendarType;
    }

    public String getLocation() {
        return _location;
    }

    public void setLocation(String _location) {
        this._location = _location;
    }

    public int getIdOwner() {
        return _idOwner;
    }

    public void setIdOwner(int _idOwner) {
        this._idOwner = _idOwner;
    }

    public int getId() {
        return _id;
    }

    public void setId(int _id) {
        this._id = _id;
    }

    public String getLoginOwner() {
        return _loginOwner;
    }

    public void setLoginOwner(String _loginOwner) {
        this._loginOwner = _loginOwner;
    }

    public String getTitleOwner() {
        return _titleOwner;
    }

    public void setTitleOwner(String _titleOwner) {
        this._titleOwner = _titleOwner;
    }

    public String getPictureOwner() {
        return _pictureOwner;
    }

    public void setPictureOwner(String _pictureOwner) {
        this._pictureOwner = _pictureOwner;
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String _title) {
        this._title = _title;
    }

    public boolean getHasToRate() {
        return _hasToRate;
    }

    public void setHasToRate(boolean _hasToRate) {
        this._hasToRate = _hasToRate;
    }

    public String getEventRegistered() {
        return _eventRegistered;
    }

    public void setEventRegistered(String _eventRegistered) {
        this._eventRegistered = _eventRegistered;
    }

    public int getIdMaker() {
        return _idMaker;
    }

    public void setIdMaker(int _idMaker) {
        this._idMaker = _idMaker;
    }

    public int getRegistered() {
        return _registered;
    }

    public void setRegistered(int _registered) {
        this._registered = _registered;
    }

    public boolean getConfirmOwner() {
        return _confirmOwner;
    }

    public void setConfirmOwner(boolean _confirmOwner) {
        this._confirmOwner = _confirmOwner;
    }

    public int getPlanningVisibleRight() {
        return _planningVisibleRight;
    }

    public void setPlanningVisibleRight(int _planningVisibleRight) {
        this._planningVisibleRight = _planningVisibleRight;
    }

    public String getLoginMaker() {
        return _loginMaker;
    }

    public void setLoginMaker(String _loginMaker) {
        this._loginMaker = _loginMaker;
    }

    public String getTitleMaker() {
        return _titleMaker;
    }

    public void setTitleMaker(String _titleMaker) {
        this._titleMaker = _titleMaker;
    }

    public boolean getConfirmMaker() {
        return _confirmMaker;
    }

    public void setConfirmMaker(boolean _confirmMaker) {
        this._confirmMaker = _confirmMaker;
    }

    public String getType() {
        return _type;
    }

    public void setType(String _type) {
        this._type = _type;
    }

    public String getTypeRoom() {
        return _typeRoom;
    }

    public void setTypeRoom(String _typeRoom) {
        this._typeRoom = _typeRoom;
    }

    public int getNbRated() {
        return _nbRated;
    }

    public void setNbRated(int _nbRated) {
        this._nbRated = _nbRated;
    }

    static public List<Susie>  parses(String json) throws JSONException {
        List<Susie> _susies = new ArrayList<Susie>();
        JSONArray object = null;
        object = new JSONArray(json);
        if (object != null)
        {
            JSONObject jsonobj = null;
            JSONObject ownerobj = null;
            JSONObject rightsobj = null;
            JSONObject makerobj = null;
            final int numberOfItems = object.length();
            for (int i = 0; i != numberOfItems; i++) {
                jsonobj = object.getJSONObject(i);
                Susie susie = new Susie();
                susie.setDuration(jsonobj.getString("duration"));
                susie.setStart(jsonobj.getString("start"));
                susie.setRatingEvent(jsonobj.getString("rating_event"));
                susie.setWeeksLeft(jsonobj.getInt("weeks_left"));
                susie.setNbPlace(jsonobj.getInt("nb_place"));
                susie.setColor(jsonobj.getString("color"));
                susie.setEnd(jsonobj.getString("end"));
                susie.setIdCalendar(jsonobj.getInt("id_calendar"));
                susie.setDescription(jsonobj.getString("description"));
                susie.setCalendarType(jsonobj.getString("calendar_type"));
                susie.setLocation(jsonobj.getString("location"));
                susie.setIdOwner(jsonobj.getInt("id_owner"));
                susie.setId(jsonobj.getInt("id"));
                susie.setTitle(jsonobj.getString("title"));
                susie.setHasToRate(jsonobj.getBoolean("has_to_rate"));
                susie.setEventRegistered(jsonobj.getString("event_registered"));
                susie.setIdMaker(jsonobj.getInt("id_maker"));
                susie.setRegistered(jsonobj.getInt("registered"));
                susie.setConfirmOwner(jsonobj.getBoolean("confirm_owner"));
                susie.setConfirmMaker(jsonobj.getBoolean("confirm_maker"));
                susie.setType(jsonobj.getString("type"));
                susie.setTypeRoom(jsonobj.getString("type_room"));
                susie.setNbRated(jsonobj.getInt("nb_rated"));


                ownerobj = jsonobj.getJSONObject("owner");
                susie.setLoginOwner(ownerobj.getString("login"));
                susie.setTitleOwner(ownerobj.getString("title"));
                susie.setPictureOwner(ownerobj.getString("picture"));

                rightsobj = jsonobj.getJSONObject("rights");
                susie.setPlanningVisibleRight(rightsobj.getInt("planning_visible"));

                makerobj = jsonobj.getJSONObject("maker");
                susie.setLoginMaker(makerobj.getString("login"));
                susie.setTitleMaker(makerobj.getString("title"));

                _susies.add(susie);
            }
        }
        return _susies;
    }

    static public Susie parse(String json) throws JSONException {
        Susie susie = new Susie();
        JSONObject jsonobj = null;
        jsonobj = new JSONObject(json);
        if (jsonobj!= null)
        {
            JSONObject ownerobj = null;
            JSONObject rightsobj = null;
            JSONObject makerobj = null;
            JSONArray  userobj = null;
            susie.setDuration(jsonobj.getString("duration"));
            susie.setStart(jsonobj.getString("start"));
            susie.setRatingEvent(jsonobj.getString("rating_event"));
            susie.setRatingStudents(jsonobj.getDouble("rating_student"));
            susie.setWeeksLeft(jsonobj.getInt("weeks_left"));
            susie.setNbPlace(jsonobj.getInt("nb_place"));
            susie.setColor(jsonobj.getString("color"));
            susie.setEnd(jsonobj.getString("end"));
            susie.setIdCalendar(jsonobj.getInt("id_calendar"));
            susie.setDescription(jsonobj.getString("description"));
            susie.setCalendarType(jsonobj.getString("calendar_type"));
            susie.setLocation(jsonobj.getString("location"));
            susie.setIdOwner(jsonobj.getInt("id_owner"));
            susie.setId(jsonobj.getInt("id"));
            susie.setTitle(jsonobj.getString("title"));
            susie.setHasToRate(jsonobj.getBoolean("has_to_rate"));
            susie.setEventRegistered(jsonobj.getString("event_registered"));
            susie.setIdMaker(jsonobj.getInt("id_maker"));
            susie.setRegistered(jsonobj.getInt("registered"));
            susie.setConfirmOwner(jsonobj.getBoolean("confirm_owner"));
            susie.setConfirmMaker(jsonobj.getBoolean("confirm_maker"));
            susie.setType(jsonobj.getString("type"));
            susie.setTypeRoom(jsonobj.getString("type_room"));
            susie.setNbRated(jsonobj.getInt("nb_rated"));


            ownerobj = jsonobj.getJSONObject("owner");
            susie.setLoginOwner(ownerobj.getString("login"));
            susie.setTitleOwner(ownerobj.getString("title"));
            susie.setPictureOwner(ownerobj.getString("picture"));

            rightsobj = jsonobj.getJSONObject("rights");
            susie.setPlanningVisibleRight(rightsobj.getInt("planning_visible"));

            makerobj = jsonobj.getJSONObject("maker");
            susie.setLoginMaker(makerobj.getString("login"));
            susie.setTitleMaker(makerobj.getString("title"));

            userobj = jsonobj.getJSONArray("logins");
            susie.setLogins(Utilisateur.parses(userobj.toString()));
        }
        return susie;
    }
}
