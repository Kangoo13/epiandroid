package com.baha.epiandroid.Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KangooRou on 26/01/2015.
 */
public class Message {

    private String _title;
    private String _userPicture;
    private String _userTitle;
    private String _userUrl;
    private String _content;
    private String _date;

    public String getTitle() {
        return _title;
    }

    public void setTitle(String _title) {
        this._title = _title;
    }

    public String getUserPicture() {
        return _userPicture;
    }

    public void setUserPicture(String _userPicture) {
        this._userPicture = _userPicture;
    }

    public String getUserTitle() {
        return _userTitle;
    }

    public void setUserTitle(String _userTitle) {
        this._userTitle = _userTitle;
    }

    public String getUserUrl() {
        return _userUrl;
    }

    public void setUserUrl(String _userurl) {
        this._userUrl = _userurl;
    }

    public String getContent() {
        return _content;
    }

    public void setContent(String _content) {
        this._content = _content;
    }

    public String getDate() {
        return _date;
    }

    public void setDate(String _date) {
        this._date = _date;
    }

    static public List<Message> parses(String json) throws JSONException {
        List<Message> _messages = new ArrayList<Message>();
        JSONArray object = null;
        object = new JSONArray(json);
        if (object != null)
        {
            JSONObject jsonobj = null;
            JSONObject userobj = null;
            final int numberOfItems = object.length();
            for (int i = 0; i != numberOfItems; i++) {
                jsonobj = object.getJSONObject(i);
                userobj = jsonobj.getJSONObject("user");

                Message message = new Message();

                message.setTitle(jsonobj.getString("title"));
                message.setUserPicture(userobj.getString("picture"));
                message.setUserTitle(userobj.getString("title"));
                message.setUserUrl(userobj.getString("url"));
                message.setContent(jsonobj.getString("content"));
                message.setDate(jsonobj.getString("date"));

                _messages.add(message);
            }
        }
        return _messages;
    }

}
